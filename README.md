# Android SDK Library Base

## Version
Current release is 1.3

## About

This initial commit includes a sample app (on the folder app) and the base of the sdklibrary (robartsdk folder).

## How to test it.

Install android development environment, and android studio IDE:

https://developer.android.com/studio/index.html

Open your IDE and then export an existing project with this repository files as source. Connect your phone with an USB cable and press the run button on the android studio interface to test te app on the phone.
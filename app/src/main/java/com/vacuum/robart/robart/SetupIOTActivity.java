package com.vacuum.robart.robart;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.vacuum.robart.robartsdk.RobartSdk;

import org.json.JSONException;
import org.json.JSONObject;

public class SetupIOTActivity extends AppCompatActivity {

    private static String STSK = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup_iot);
    }

    /**
     * Implementation of registerIOT SDK method, for more info read the SDK documentation
     *
     * @param View Sample View
     * @return Void
     */
    public void registerIOT(View View) {
        final TextView outputView = (TextView) findViewById(R.id.showOutput);
        outputView.setText("Register on IoT server...");

        final Context context = this;
        final String user_id = "c";
        final String robot_id = "robby32";
        final String robot_password = "default-secret";

        RobartSdk.SuccessCallback stskSuccess = new RobartSdk.SuccessCallback() {
            @Override
            public void onSuccess(String new_stsk) {
                RobartSdk.registerIOT(context, user_id, new_stsk, robot_id, robot_password,
                        new RobartSdk.SuccessCallback() {
                            @Override
                            public void onSuccess(String result) {
                                if (result.length() == 0) {
                                    result = "Completed";
                                }
                                outputView.setText(result);
                            }
                        },
                        new RobartSdk.ErrorCallback() {
                            @Override
                            public void onError(String result) {
                                outputView.setText(result);
                            }
                        }
                );

            }
        };
        RobartSdk.ErrorCallback stskError = new RobartSdk.ErrorCallback() {
            @Override
            public void onError(String error) {
                outputView.setText(error);
            }
        };
        getSTSK(user_id, stskSuccess, stskError);
    }


    /* Test of initConnection SDK method, for more info read the SDK documentation
    * @param View Sample View
    * @return Void
    */
    public void initializeSDKIOTConnection(View View) {
        final TextView outputView = (TextView) findViewById(R.id.showOutput);
        outputView.setText("Test: init connection");
        RobartSdk.initializeSDKIOTConnection(this,
                new RobartSdk.SuccessCallback() {
                    @Override
                    public void onSuccess(String result) {
                        if (result.length() == 0) {
                            result = "Completed";
                        }
                        outputView.setText(result);
                    }
                },
                new RobartSdk.ErrorCallback() {
                    @Override
                    public void onError(String result) {
                        outputView.setText(result);
                    }
                }
        );
    }

    /* Implementation of robotGetRobotStatus SDK method, for more info read the SDK documentation
    * @param View Sample View
    * @return Void
    */
    public void robotGetRobotStatusIOT(View View) {
        final TextView outputView = (TextView) findViewById(R.id.showOutput);
        outputView.setText("Requesting over IOT...");
        JSONObject parameters = new JSONObject();
        String user30 = "c";

        RobartSdk.robotGetRobotStatus(this, parameters,
                new RobartSdk.SuccessCallback() {
                    @Override
                    public void onSuccess(String result) {
                        outputView.setText(result);
                    }
                },
                new RobartSdk.ErrorCallback() {
                    @Override
                    public void onError(String result) {
                        outputView.setText(result);
                    }
                },
                user30, "robby32"
        );

    }

    /*
    * Test of the generatePLTSK() method;
    * @param View Sample View
    * @return Void
    */
    public void testPLTSK(View View) {
        final TextView outputView = (TextView) findViewById(R.id.showOutput);
        String PLTSK = RobartSdk.testPLTSK();
        outputView.setText("GENERATED PLTSK: \n" + PLTSK);
    }

    ;

    /*
    * Test of the getSTSK() method;
    * @param View Sample View
    * @return Void
    */
    public void testSTSK(View View) {
        final TextView outputView = (TextView) findViewById(R.id.showOutput);

        RobartSdk.SuccessCallback stskSuccess = new RobartSdk.SuccessCallback() {
            @Override
            public void onSuccess(String result) {
                outputView.setText("NEW STSK: \n" + result);
            }
        };
        RobartSdk.ErrorCallback stskError = new RobartSdk.ErrorCallback() {
            @Override
            public void onError(String error) {
                outputView.setText(error);
            }
        };
        getSTSK("c", stskSuccess, stskError);
    }

    ;


    /*
    * Used to get the STSK from the B2B API
    * @param user (String) The user the whom the STSK will be generated
    * @return Void
    */
    public void getSTSK(String user, final RobartSdk.SuccessCallback callback, final RobartSdk.ErrorCallback failure) {
        if (STSK != null) {
            callback.onSuccess(STSK);
            return;
        }
        String endpoint = String.format("https://52.57.14.46:443/iot/b2b-api/users/%s/stsk", user);
        JSONObject auth = new JSONObject();
        try {
            auth.put("user", "chris");
            auth.put("password", "H4ppy_SDK_cod!ng");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Request.POST(this, endpoint, auth,
                new Request.SuccessCallback() {
                    @Override
                    public void onSuccess(String result) {
                        JSONObject jsonObj = null;
                        String stskToken = null;
                        try {
                            jsonObj = new JSONObject(result);
                            stskToken = jsonObj.get("stsk").toString();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            failure.onError(e.toString());
                        }
                        if (stskToken != null) {
                            STSK = stskToken;
                        }
                        callback.onSuccess(STSK);
                    }
                },
                new Request.ErrorCallback() {
                    @Override
                    public void onError(String result) {
                        failure.onError(result);
                    }
                }
        );
    }

    ;

}

package com.vacuum.robart.robart;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.vacuum.robart.robartsdk.RobartSdk;

import org.json.JSONException;
import org.json.JSONObject;

public class TestPermanentMapActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_permanent_map);
    }

    /////////////////////////////////////////////////////////////
    // permanent map
    /////////////////////////////////////////////////////////////

    /**
     * * Implementation of actionMoveToTargetPoint SDK method, for more info read the SDK documentation
    * @param View Sample View
    * @return Void
    */
    public void actionMoveToTargetPoint(View View) {
        final TextView outputView = (TextView) findViewById(R.id.permanentMapshowOutput);
        outputView.setText("Requesting...");
        JSONObject parameters = new JSONObject();

        try {
            parameters.put("map_id", 4);
            parameters.put("x1", -100);
            parameters.put("y1", 20.4);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RobartSdk.actionMoveToTargetPoint(this, parameters,
                new RobartSdk.SuccessCallback(){
                    @Override
                    public void onSuccess(String result){
                        outputView.setText(result);
                    }
                },
                new RobartSdk.ErrorCallback(){
                    @Override
                    public void onError(String result){
                        outputView.setText(result);
                    }
                }
        );

    }

    /**
     * * Implements non-Iot-style actionExplore
    * @param View
    */
    public void actionExplore(View View){
        final TextView outputView = (TextView) findViewById(R.id.permanentMapshowOutput);
        outputView.setText("Requesting...");

        RobartSdk.actionExplore(this, new RobartSdk.SuccessCallback(){
                    @Override
                    public void onSuccess(String result){
                        outputView.setText(result);
                    }
                },
                new RobartSdk.ErrorCallback(){
                    @Override
                    public void onError(String result){
                        outputView.setText(result);
                    }
                });
    }

    public void robotGetCurrentMapId(View View){
        final TextView outputView = (TextView) findViewById(R.id.permanentMapshowOutput);
        outputView.setText("Requesting...");

        RobartSdk.robotGetCurrentMapId(this, new RobartSdk.SuccessCallback(){
                    @Override
                    public void onSuccess(String result){
                        outputView.setText(result);
                    }
                },
                new RobartSdk.ErrorCallback(){
                    @Override
                    public void onError(String result){
                        outputView.setText(result);
                    }
                });
    }

    /**
     * Save a the current Map, if a particular map should be saved put the respective map_id into parameters
     * @param view
     */
    public void robotSaveMap(View view){
        final TextView outputView = (TextView) findViewById(R.id.permanentMapshowOutput);
        outputView.setText("Requesting...");

        JSONObject parameters = new JSONObject();


        RobartSdk.robotSaveMap(this, parameters, new RobartSdk.SuccessCallback(){

            @Override
            public void onSuccess(String result) {
                outputView.setText(result);
            }
        }, new RobartSdk.ErrorCallback(){

            @Override
            public void onError(String result) {
                outputView.setText(result);
            }
        });
    }

    /**
            * Implementation of actionCleanMap SDK method, for more info read the SDK documentation
    * @param View Sample View
    * @return Void
    */
    public void actionCleanMap(View View) {
        final TextView outputView = (TextView) findViewById(R.id.permanentMapshowOutput);
        outputView.setText("Requesting...");
        JSONObject parameters = new JSONObject();

        try {
            parameters.put("map_id", 7);
            parameters.put("cleaning_parameter_set", 0);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RobartSdk.actionCleanMap(this, parameters,
                new RobartSdk.SuccessCallback(){
                    @Override
                    public void onSuccess(String result){
                        outputView.setText(result);
                    }
                },
                new RobartSdk.ErrorCallback(){
                    @Override
                    public void onError(String result){
                        outputView.setText(result);
                    }
                }
        );

    }

    /**
            * Implementation of actionCleanArea SDK method, for more info read the SDK documentation
    * @param View Sample View
    * @return Void
    */
    public void actionCleanArea(View View) {
        final TextView outputView = (TextView) findViewById(R.id.permanentMapshowOutput);
        outputView.setText("Requesting...");
        JSONObject parameters = new JSONObject();

        try {
            parameters.put("map_id", 1);
            parameters.put("area_id", 1);
            parameters.put("area_id", 2);
            parameters.put("cleaning_parameter_set", 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RobartSdk.actionCleanArea(this, parameters,
                new RobartSdk.SuccessCallback(){
                    @Override
                    public void onSuccess(String result){
                        outputView.setText(result);
                    }
                },
                new RobartSdk.ErrorCallback(){
                    @Override
                    public void onError(String result){
                        outputView.setText(result);
                    }
                }
        );

    }


    /**
            * Implementation of robotAddArea SDK method, for more info read the SDK documentation
    * @param View Sample View
    * @return Void
    */
    public void robotAddArea(View View) {
        final TextView outputView = (TextView) findViewById(R.id.permanentMapshowOutput);
        outputView.setText("Requesting...");
        JSONObject parameters = new JSONObject();

        try {
            parameters.put("map_id", 1);
            parameters.put("area_meta_data", "meineArea");
            parameters.put("area_type", "blocking");
            parameters.put("cleaning_parameter_set", 1);
            parameters.put("x1", -100);
            parameters.put("y1", -100);
            parameters.put("x2", -200);
            parameters.put("y2", -100);
            parameters.put("x3", -200);
            parameters.put("y3", -200);
            parameters.put("x4", -200);
            parameters.put("y4", -100);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RobartSdk.robotAddArea(this, parameters,
                new RobartSdk.SuccessCallback(){
                    @Override
                    public void onSuccess(String result){
                        outputView.setText(result);
                    }
                },
                new RobartSdk.ErrorCallback(){
                    @Override
                    public void onError(String result){
                        outputView.setText(result);
                    }
                }
        );

    }

    /**
            * Implementation of robotMergeAreas SDK method, for more info read the SDK documentation
    * @param View Sample View
    * @return Void
    */
    public void robotMergeAreas(View View) {
        final TextView outputView = (TextView) findViewById(R.id.permanentMapshowOutput);
        outputView.setText("Requesting...");
        JSONObject parameters = new JSONObject();

        try {
            parameters.put("map_id", 7);
            parameters.put("area_meta_data", "meineArea");
            parameters.put("area_type", "to_be_cleaned");
            parameters.put("cleaning_parameter_set", 1);
            parameters.put("area_id1", 1);
            parameters.put("area_id2", 2);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RobartSdk.robotMergeAreas(this, parameters,
                new RobartSdk.SuccessCallback(){
                    @Override
                    public void onSuccess(String result){
                        outputView.setText(result);
                    }
                },
                new RobartSdk.ErrorCallback(){
                    @Override
                    public void onError(String result){
                        outputView.setText(result);
                    }
                }
        );

    }
    /**
     * * Implementation of robotDeleteArea SDK method, for more info read the SDK documentation
    * @param View Sample View
    * @return Void
    */
    public void robotDeleteArea(View View) {
        final TextView outputView = (TextView) findViewById(R.id.permanentMapshowOutput);
        outputView.setText("Requesting...");
        JSONObject parameters = new JSONObject();

        try {
            parameters.put("map_id", 7);
            parameters.put("area_id", 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RobartSdk.robotDeleteArea(this, parameters,
                new RobartSdk.SuccessCallback(){
                    @Override
                    public void onSuccess(String result){
                        outputView.setText(result);
                    }
                },
                new RobartSdk.ErrorCallback(){
                    @Override
                    public void onError(String result){
                        outputView.setText(result);
                    }
                }
        );

    }

    /**
            * Implementation of robotSplitArea SDK method, for more info read the SDK documentation
    * @param View Sample View
    * @return Void
    */
    public void robotSplitArea(View View) {
        final TextView outputView = (TextView) findViewById(R.id.permanentMapshowOutput);
        outputView.setText("Requesting...");
        JSONObject parameters = new JSONObject();

        try {
            parameters.put("map_id", 7);
            parameters.put("area_id", 1);
            parameters.put("x1", -628);
            parameters.put("y1", 192);
            parameters.put("x2", 388);
            parameters.put("y2", -539);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        RobartSdk.robotSplitArea(this, parameters,
                new RobartSdk.SuccessCallback(){
                    @Override
                    public void onSuccess(String result){
                        outputView.setText(result);
                    }
                },
                new RobartSdk.ErrorCallback(){
                    @Override
                    public void onError(String result){
                        outputView.setText(result);
                    }
                }
        );

    }
    /**
            * Implementation of robotSetUsedMap SDK method, for more info read the SDK documentation
    * @param View Sample View
    * @return Void
    */
    public void robotSetUsedMap(View View) {
        final TextView outputView = (TextView) findViewById(R.id.permanentMapshowOutput);
        outputView.setText("Requesting...");
        JSONObject parameters = new JSONObject();

        try {
            parameters.put("map_id", 7);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RobartSdk.robotSetUsedMap(this, parameters,
                new RobartSdk.SuccessCallback(){
                    @Override
                    public void onSuccess(String result){
                        outputView.setText(result);
                    }
                },
                new RobartSdk.ErrorCallback(){
                    @Override
                    public void onError(String result){
                        outputView.setText(result);
                    }
                }
        );

    }

    /**
            * Implementation of mapModifyMap SDK method, for more info read the SDK documentation
    * @param View Sample View
    * @return Void
    */
    public void mapModifyMap(View View) {
        final TextView outputView = (TextView) findViewById(R.id.permanentMapshowOutput);
        outputView.setText("Requesting...");
        JSONObject parameters = new JSONObject();

        try {
            parameters.put("map_id", 7);
            parameters.put("map_meta_data", "Meine super Map");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RobartSdk.mapModifyMap(this, parameters,
                new RobartSdk.SuccessCallback(){
                    @Override
                    public void onSuccess(String result){
                        outputView.setText(result);
                    }
                },
                new RobartSdk.ErrorCallback(){
                    @Override
                    public void onError(String result){
                        outputView.setText(result);
                    }
                }
        );

    }

    /**
            * Implementation of robotDeleteMap SDK method, for more info read the SDK documentation
    * @param View Sample View
    * @return Void
    */
    public void robotDeleteMap(View View) {
        final TextView outputView = (TextView) findViewById(R.id.permanentMapshowOutput);
        outputView.setText("Requesting...");
        JSONObject parameters = new JSONObject();

        try {
            parameters.put("map_id", 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RobartSdk.robotDeleteMap(this, parameters,
                new RobartSdk.SuccessCallback(){
                    @Override
                    public void onSuccess(String result){
                        outputView.setText(result);
                    }
                },
                new RobartSdk.ErrorCallback(){
                    @Override
                    public void onError(String result){
                        outputView.setText(result);
                    }
                }
        );

    }

    /**
            * Implementation of mapGetAreas SDK method, for more info read the SDK documentation
    * @param View Sample View
    * @return Void
    */
    public void mapGetAreas(View View) {
        final TextView outputView = (TextView) findViewById(R.id.permanentMapshowOutput);
        outputView.setText("Requesting...");
        JSONObject parameters = new JSONObject();

        try {
            parameters.put("map_id", 7);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RobartSdk.mapGetAreas(this, parameters,
                new RobartSdk.SuccessCallback(){
                    @Override
                    public void onSuccess(String result){
                        outputView.setText(result);
                    }
                },
                new RobartSdk.ErrorCallback(){
                    @Override
                    public void onError(String result){
                        outputView.setText(result);
                    }
                }
        );

    }

    /**
            * Implementation of mapGetTileMap SDK method, for more info read the SDK documentation
    * @param View Sample View
    * @return Void
    */
    public void mapGetTileMap(View View) {
        final TextView outputView = (TextView) findViewById(R.id.permanentMapshowOutput);
        outputView.setText("Requesting...");
        JSONObject parameters = new JSONObject();

//        try {
//            parameters.put("map_id", 7);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }

        RobartSdk.mapGetTileMap(this, parameters,
                new RobartSdk.SuccessCallback(){
                    @Override
                    public void onSuccess(String result){
                        outputView.setText(result);
                    }
                },
                new RobartSdk.ErrorCallback(){
                    @Override
                    public void onError(String result){
                        outputView.setText(result);
                    }
                }
        );

    }


    /**
            * Implementation of mapGetDoorMap SDK method, for more info read the SDK documentation
    * @param View Sample View
    * @return Void
    */
    public void mapGetDoorMap(View View) {
        final TextView outputView = (TextView) findViewById(R.id.permanentMapshowOutput);
        outputView.setText("Requesting...");
        JSONObject parameters = new JSONObject();

        try {
            parameters.put("map_id", 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RobartSdk.mapGetDoorMap(this, parameters,
                new RobartSdk.SuccessCallback(){
                    @Override
                    public void onSuccess(String result){
                        outputView.setText(result);
                    }
                },
                new RobartSdk.ErrorCallback(){
                    @Override
                    public void onError(String result){
                        outputView.setText(result);
                    }
                }
        );

    }

    /**
            * Implementation of robotGetMaps SDK method, for more info read the SDK documentation
    * @param View Sample View
    * @return Void
    */
    public void robotGetMaps(View View) {
        final TextView outputView = (TextView) findViewById(R.id.permanentMapshowOutput);
        outputView.setText("Requesting...");

        RobartSdk.robotGetMaps(this,
                new RobartSdk.SuccessCallback(){
                    @Override
                    public void onSuccess(String result){
                        outputView.setText(result);
                    }
                },
                new RobartSdk.ErrorCallback(){
                    @Override
                    public void onError(String result){
                        outputView.setText(result);
                    }
                }
        );

    }
}

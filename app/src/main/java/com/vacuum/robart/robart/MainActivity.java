/**
 * Copyright (c) 2016 Robart
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including the rights to use, copy, modify,
 * merge, publish, distribute, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @author Robart
 * @version 1.3
 * @since 2016-07-05
 */

package com.vacuum.robart.robart;

import android.app.Activity;
import android.content.Intent;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vacuum.robart.robart.discovery.NsdHelper;
import com.vacuum.robart.robartsdk.RobartSdk;

import org.json.JSONObject;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * Sample app which contains the RobartSdk library implementation.
 */
public class MainActivity extends Activity {


    private final String TAG = this.getClass().getSimpleName();
    NsdHelper mNsdHelper;
    private ArrayBlockingQueue<NsdServiceInfo> mResolvedServices;

    /**
     * Code to be executed onCreate of MainActivity for mexample App. Withing it the
     * RobartSdk.robotSelect() method is called.
     * @param savedInstanceState
     * @return Void
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //RobartSdk.robotSelect("http://192.168.0.107:10009/");
        mResolvedServices = new ArrayBlockingQueue<NsdServiceInfo>(1);

    }


    public void doMDNSBrowse(View view) {
        //final TextView outputView = (TextView) findViewById(R.id.showOutput);
        //outputView.setText("Searching for Robots...");
        mNsdHelper.discoverServices();

        ArrayList<NsdServiceInfo> services = mNsdHelper.getUnresolvedServices();
        //printServiceToOutput(services);

        //generate Buttons to connect to each robot
        listFoundRobotsAsButtons(services);
    }

    private void listFoundRobotsAsButtons(ArrayList<NsdServiceInfo> services) {
        LinearLayout ll = (LinearLayout) findViewById(R.id.listOfFoundRobots);
        //delete alle existing Buttons
        ll.removeAllViews();
        //add new buttons
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        for (final NsdServiceInfo currService : services) {
            Button uniqueIdButton = new Button(this);
            uniqueIdButton.setText(currService.getServiceName());
            uniqueIdButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final TextView outputView = (TextView) findViewById(R.id.showOutput);
                    mResolvedServices.clear();
                    mNsdHelper.getMNSDManager().resolveService(currService, new RobotServicesResolveListener());

                    try {
                        InetAddress host = mResolvedServices.take().getHost();
                        outputView.setText("" + host);
                        RobartSdk.robotSelect("http:/" + host + ":10009/");
                        requestRobotsId();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
            ll.addView(uniqueIdButton, lp);
        }
    }


    private class RobotServicesResolveListener implements NsdManager.ResolveListener {

        @Override
        public void onResolveFailed(NsdServiceInfo serviceInfo, int errorCode) {
            switch (errorCode) {
                case NsdManager.FAILURE_ALREADY_ACTIVE:
                    Log.e(TAG, "FAILURE_ALREADY_ACTIVE");
                    // Just try again...
                    //startResolveService(serviceInfo);
                    break;
                case NsdManager.FAILURE_INTERNAL_ERROR:
                    Log.e(TAG, "FAILURE_INTERNAL_ERROR");
                    break;
                case NsdManager.FAILURE_MAX_LIMIT:
                    Log.e(TAG, "FAILURE_MAX_LIMIT");
                    break;
            }
            mResolvedServices.clear();
        }

        @Override
        public void onServiceResolved(NsdServiceInfo serviceInfo) {
            Log.e(TAG, "Resolve Succeeded. " + serviceInfo);
            mResolvedServices.add(serviceInfo);
        }
    }

    public void requestRobotsId() {
        final TextView outputView = (TextView) findViewById(R.id.showOutput);
        outputView.setText("Requesting...");
        JSONObject parameters = new JSONObject();
        RobartSdk.robotGetRobotId(this, parameters,
                new RobartSdk.SuccessCallback() {
                    @Override
                    public void onSuccess(String result) {
                        outputView.setText(result);
                        Button connectToRobot = (Button) findViewById(R.id.connectToRobot);
                        connectToRobot.setVisibility(View.VISIBLE);
                    }
                },
                new RobartSdk.ErrorCallback() {
                    @Override
                    public void onError(String result) {
                        //outputView.setText(result);
                        outputView.append("\n"+result);
                        Button connectToRobot = (Button) findViewById(R.id.connectToRobot);
                        connectToRobot.setVisibility(View.INVISIBLE);
                    }
                },
                null, null
        );
    }

    /**
     * A robot is selected. Show Main Menu Activity
     * @param view
     */
    public void useThisRobotAndSwitchToMainMenu(View view) {
        mNsdHelper.stopDiscovery();
        Intent intent = new Intent(this, MainMenuActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onStart() {
        Log.d(TAG, "Starting.");
        mNsdHelper = new NsdHelper(this);
        super.onStart();
    }


    @Override
    protected void onPause() {
        Log.d(TAG, "Pausing.");
        if (mNsdHelper != null) {
            mNsdHelper.stopDiscovery();
        }
        super.onPause();
    }

    @Override
    protected void onResume() {
        Log.d(TAG, "Resuming.");
        super.onResume();
        if (mNsdHelper != null) {
            mNsdHelper.discoverServices();
            ArrayList<NsdServiceInfo> service = mNsdHelper.getUnresolvedServices();
            if (service != null)
                printServiceToOutput(service);
        }
    }

    private void printServiceToOutput(ArrayList<NsdServiceInfo> service) {
        final TextView outputView = (TextView) findViewById(R.id.showOutput);
        if (outputView != null) {
            outputView.setText(service.toString());
        }
    }


    @Override
    protected void onStop() {
        Log.d(TAG, "Being stopped.");
        if (mNsdHelper != null) {
            mNsdHelper.stopDiscovery();
            mNsdHelper = null;
        }
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "Being destroyed.");
        if (mNsdHelper != null) {
            mNsdHelper.stopDiscovery();
            mNsdHelper = null;
        }
        super.onDestroy();
    }


}
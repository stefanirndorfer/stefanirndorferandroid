/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.vacuum.robart.robart.discovery;

import android.content.Context;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;
import android.util.Log;
import java.util.ArrayList;
import java.util.Iterator;


public class NsdHelper {

    Context mContext;
    NsdManager mNsdManager;
    NsdManager.DiscoveryListener mDiscoveryListener;


    public static final String SERVICE_TYPE = "_aicu-http._tcp.";
    public final String TAG = this.getClass().getSimpleName();

    private ArrayList<NsdServiceInfo> mServices;


    public NsdHelper(Context context) {
        mContext = context;
        mServices = new ArrayList<>();
        mNsdManager = (NsdManager) context.getSystemService(Context.NSD_SERVICE);
    }

    /**
     * fills the list mServices with all services with Service Type _aicu-http._tcp
     */
    public void initializeDiscoveryListener() {
        mDiscoveryListener = new NsdManager.DiscoveryListener() {

            @Override
            public void onDiscoveryStarted(String regType) {
                Log.d(TAG, "Service discovery started");
            }

            @Override
            public void onServiceFound(NsdServiceInfo service) {
                Log.d(TAG, "Service discovery success" + service);
                if (!service.getServiceType().equals(SERVICE_TYPE)) {
                    Log.d(TAG, "Unknown Service Type: " + service.getServiceType());
                } else {
                    Log.d(TAG, "Found Service: " + service.getServiceType());
                    addToServicesListIfNotAlreadyContained(service);
                }
            }

            @Override
            public void onServiceLost(NsdServiceInfo service) {
                Log.e(TAG, "service lost" + service.toString());

                Iterator<NsdServiceInfo> it = mServices.iterator();
                while (it.hasNext()){
                    if (it.next().getServiceName().equals(service.getServiceName())){
                        it.remove();
                        break;
                    }
                }
            }

            @Override
            public void onDiscoveryStopped(String serviceType) {
                Log.i(TAG, "Discovery stopped: " + serviceType);
            }

            @Override
            public void onStartDiscoveryFailed(String serviceType, int errorCode) {
                Log.e(TAG, "Discovery failed: Error code:" + errorCode);
            }

            @Override
            public void onStopDiscoveryFailed(String serviceType, int errorCode) {
                Log.e(TAG, "Discovery failed: Error code:" + errorCode);
            }
        };
    }

    /**
     * makes sure that services are not listed twice
     * @param service
     */
    private void addToServicesListIfNotAlreadyContained(NsdServiceInfo service) {
        boolean found = false;
        Iterator it = mServices.iterator();
        while (it.hasNext()){
            NsdServiceInfo currentElem = (NsdServiceInfo) it.next();
            if(currentElem.getServiceName().equals(service.getServiceName())){
                found = true;
                break;
            }
        }
        if(!found)
            mServices.add(service);
    }


    /**
     * triggers a new discovery
     */
    public void discoverServices() {
        stopDiscovery();  // Cancel any existing discovery request
       initializeDiscoveryListener();
        mNsdManager.discoverServices(
                SERVICE_TYPE, NsdManager.PROTOCOL_DNS_SD, mDiscoveryListener);
    }

    public void stopDiscovery() {
        if (mDiscoveryListener != null) {
            try {
                mNsdManager.stopServiceDiscovery(mDiscoveryListener);
            } finally {
            }
            mDiscoveryListener = null;
        }
    }

    public synchronized ArrayList<NsdServiceInfo> getUnresolvedServices() {
        return mServices;
    }

    public synchronized ArrayList<NsdServiceInfo> getFoundServices() {
        return mServices;
    }

    public synchronized void setFoundServices( ArrayList<NsdServiceInfo> services) {
        mServices = services;
    }

    public NsdManager getMNSDManager() {
        return mNsdManager;
    }

}

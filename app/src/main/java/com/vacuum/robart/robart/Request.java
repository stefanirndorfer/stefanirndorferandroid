/**
 * Copyright (c) 2016 Robart
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including the rights to use, copy, modify,
 * merge, publish, distribute, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @author  Robart
 * @version 1.3
 * @since   2016-07-05
 */
package com.vacuum.robart.robart;

import android.content.Context;
import android.util.Base64;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

/**
 * Request class used to manage Http Requests
 */
public class Request {

    /**
     * Executes a http request over the post method
     * @param context Current context
     * @param endpoint Request url
     * @param auth Authorization credentials (if any)
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @return void
     */
    public static void POST(Context context,  String endpoint, JSONObject auth, final SuccessCallback callback, final ErrorCallback failure) {
        final RequestQueue queue = Volley.newRequestQueue(context);
        String authorization = null;
        if (auth != null && auth.length() > 0) {
            try {
                authorization = "Basic " + Base64.encodeToString(String.format("%s:%s",auth.get("user").toString(),auth.get("password").toString()).getBytes(), Base64.NO_WRAP);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        final String finalAuthorization = authorization;
        HttpsTrustManager.allowAllSSL();
        final StringRequest stringRequest = new StringRequest(com.android.volley.Request.Method.POST, endpoint,
                new Response.Listener<String>() {
                    public String result;
                    @Override
                    public void onResponse(String response) {
                        result = response;
                        callback.onSuccess(result);
                    }
                },
                new Response.ErrorListener() {
                    public String result;
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        if(volleyError.networkResponse != null && volleyError.networkResponse.data != null){
                            VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                            volleyError = error;
                        }
                        result = volleyError.getMessage();
                        failure.onError(result);
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                if (finalAuthorization != null) {
                    params.put("Authorization", finalAuthorization);
                }
                return params;
            }
        };
        int socketTimeout = 900000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        queue.add(stringRequest);
    };

    /**
     * A public interface to execute SuccessCallbacks from the main thread.
     * @return void
     */
    public interface SuccessCallback{
        void onSuccess(String result);
    }

    /**
     * A public interface to execute SuccessCallbacks from the main thread.
     * @return void
     */
    public interface ErrorCallback{
        void onError(String result);
    }

}

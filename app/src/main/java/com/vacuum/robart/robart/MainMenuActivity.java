package com.vacuum.robart.robart;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainMenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
    }

    public void testInfrastructureMode(View view){
        Intent intent = new Intent(this, TestInfrastructureModeActivity.class);
        startActivity(intent);
    }

    public void testIoT(View view){
        Intent intent = new Intent(this, SetupIOTActivity.class);
        startActivity(intent);
    }

    public void testPermanentMap(View view){
        Intent intent = new Intent(this, TestPermanentMapActivity.class);
        startActivity(intent);
    }
}

package com.vacuum.robart.robart.discovery;

import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;
import android.util.Log;

/**
 * Created by stefan on 17.10.2016.
 */
public class NsdResolveListener  implements NsdManager.ResolveListener{

    private final String TAG = this.getClass().getSimpleName();
    NsdHelper nsdHelper;

    public NsdResolveListener(NsdHelper nsdHelper) {
        this.nsdHelper = nsdHelper;
    }

    @Override
    public void onResolveFailed(NsdServiceInfo serviceInfo, int errorCode) {
        Log.e(TAG, "Resolve failed" + errorCode);
    }

    @Override
    public void onServiceResolved(NsdServiceInfo serviceInfo) {
        Log.e(TAG, "Resolve Succeeded. " + serviceInfo);
        addToKnownServices(serviceInfo);
    }

    /**
     * proves if a service is already known. If not it will be added to mServices
     * @param serviceInfo
     */
    private synchronized void addToKnownServices(NsdServiceInfo serviceInfo) {
        boolean contained = false;
        for (int i = 0; i < nsdHelper.getFoundServices().size(); i++){
            if (nsdHelper.getFoundServices().get(i).getServiceName().equals(serviceInfo.getServiceName())){
                contained = true;
                break;
            }
        }
        if (!contained)
            nsdHelper.getFoundServices().add(serviceInfo);
    }
}

package com.vacuum.robart.robart;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.vacuum.robart.robartsdk.RobartSdk;

import org.json.JSONException;
import org.json.JSONObject;

public class TestInfrastructureModeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_infrastructure_mode);
    }


    /**
            * Implementation of checkFirmwareVersion SDK method, for more info read the SDK documentation
    * @param View Sample View
    * @return Void
    */
    public void checkFirmwareVersion(View View) {
        final TextView outputView = (TextView) findViewById(R.id.showOutput);
        outputView.setText("Checking version...");
        JSONObject parameters = new JSONObject();
        RobartSdk.checkFirmwareVersion(this, parameters,
                new RobartSdk.SuccessCallback(){
                    @Override
                    public void onSuccess(String result){
                        outputView.setText(result);
                    }
                },
                new RobartSdk.ErrorCallback(){
                    @Override
                    public void onError(String result){
                        outputView.setText(result);
                    }
                },
                null, null
        );
    }

    /**
            * Implementation of add_robot SDK method, for more info read the SDK documentation
    * @param View Sample View
    * @return Void
    */
    public void downloadFirmware(View View) {
        if (isStoragePermissionGranted()){
            startDownload();
        }
    }

    /**
            * Validates if permissions has been granted. Running permissions required for android 6.0.
            * @return Void
    */
    public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                System.out.println("Permission is granted");
                return true;
            } else {
                System.out.println("Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else {
            System.out.println("Permission is granted");
            return true;
        }
    }

    /**
            * for android api lvl 23 + (6.0) to be executed after permissions are granted.
    * @return Void
    */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults[0]== PackageManager.PERMISSION_GRANTED){
            System.out.println("Permission: "+permissions[0]+ "was "+grantResults[0]);
            startDownload();
        }
    }

    /**
     * Private method used to Start the download when permissions are granted
    * @return Void
    */
    private void startDownload() {
        final TextView outputView = (TextView) findViewById(R.id.showOutput);
        outputView.setText("Checking version...");
        JSONObject parameters = new JSONObject();

        RobartSdk.downloadFirmware(this, parameters, new RobartSdk.SuccessCallback(){
                    @Override
                    public void onSuccess(String result){
                        outputView.setText(result);
                    }
                },
                new RobartSdk.ErrorCallback(){
                    @Override
                    public void onError(String result){
                        outputView.setText(result);
                    }
                },
                new RobartSdk.ProgressCallback(){
                    @Override
                    public void onProgress(int result){
                        outputView.setText(Integer.toString(result) + "%");
                    }
                },
                null, null
        );
    }

    /**
            * Implementation of installFirmware SDK method, for more info read the SDK documentation
    * @param View Sample View
    * @return Void
    */
    public void installFirmware(View View) {
        final TextView outputView = (TextView) findViewById(R.id.showOutput);
        outputView.setText("Installing firmware...");
        RobartSdk.installFirmware(this,
                new RobartSdk.SuccessCallback(){
                    @Override
                    public void onSuccess(String result){
                        outputView.setText(result);
                    }
                },
                new RobartSdk.ErrorCallback(){
                    @Override
                    public void onError(String result){
                        outputView.setText(result);
                    }
                },
                null, null
        );
    }


    /**
     * Implementation of robotGetRobotStatus SDK method, for more info read the SDK documentation
     *
     * @param View Sample View
     * @return Void
     */
    public void robotGetRobotStatus(View View) {
        final TextView outputView = (TextView) findViewById(R.id.showOutput);
        outputView.setText("Requesting...");
        JSONObject parameters = new JSONObject();

        RobartSdk.robotGetRobotStatus(this, parameters,
                new RobartSdk.SuccessCallback() {
                    @Override
                    public void onSuccess(String result) {
                        outputView.setText(result);
                    }
                },
                new RobartSdk.ErrorCallback() {
                    @Override
                    public void onError(String result) {
                        outputView.setText(result);
                    }
                },
                null, null
        );

    }

    /**
     * Implementation of robotGetRobotName SDK method, for more info read the SDK documentation
     *
     * @param View Sample View
     * @return Void
     */
    public void robotGetRobotName(View View) {
        final TextView outputView = (TextView) findViewById(R.id.showOutput);
        outputView.setText("Requesting...");
        JSONObject parameters = new JSONObject();

        RobartSdk.robotGetRobotName(this, parameters,
                new RobartSdk.SuccessCallback() {
                    @Override
                    public void onSuccess(String result) {
                        outputView.setText(result);
                    }
                },
                new RobartSdk.ErrorCallback() {
                    @Override
                    public void onError(String result) {
                        outputView.setText(result);
                    }
                },
                null, null
        );

    }

    /**
     * Implementation of robotGetRobotId SDK method, for more info read the SDK documentation
     *
     * @param View Sample View
     * @return Void
     */
    public void robotGetRobotId(View View) {
        final TextView outputView = (TextView) findViewById(R.id.showOutput);
        outputView.setText("Requesting...");
        JSONObject parameters = new JSONObject();

        RobartSdk.robotGetRobotId(this, parameters,
                new RobartSdk.SuccessCallback() {
                    @Override
                    public void onSuccess(String result) {
                        outputView.setText(result);
                    }
                },
                new RobartSdk.ErrorCallback() {
                    @Override
                    public void onError(String result) {
                        outputView.setText(result);
                    }
                },
                null, null
        );

    }

    /**
     * Implementation of actionGoHome SDK method, for more info read the SDK documentation
     *
     * @param View Sample View
     * @return Void
     */
    public void actionGoHome(View View) {
        final TextView outputView = (TextView) findViewById(R.id.showOutput);
        outputView.setText("Requesting...");
        JSONObject parameters = new JSONObject();

        RobartSdk.actionGoHome(this, parameters,
                new RobartSdk.SuccessCallback() {
                    @Override
                    public void onSuccess(String result) {
                        outputView.setText(result);
                    }
                },
                new RobartSdk.ErrorCallback() {
                    @Override
                    public void onError(String result) {
                        outputView.setText(result);
                    }
                },
                null, null
        );

    }

    /**
     * Implementation of actionStop SDK method, for more info read the SDK documentation
     *
     * @param View Sample View
     * @return Void
     */
    public void actionStop(View View) {
        final TextView outputView = (TextView) findViewById(R.id.showOutput);
        outputView.setText("Requesting...");
        JSONObject parameters = new JSONObject();

        RobartSdk.actionStop(this, parameters,
                new RobartSdk.SuccessCallback() {
                    @Override
                    public void onSuccess(String result) {
                        outputView.setText(result);
                    }
                },
                new RobartSdk.ErrorCallback() {
                    @Override
                    public void onError(String result) {
                        outputView.setText(result);
                    }
                },
                null, null
        );

    }

    /**
     * Implementation of actionClean SDK method, for more info read the SDK documentation
     *
     * @param View Sample View
     * @return Void
     */
    public void actionClean(View View) {
        final TextView outputView = (TextView) findViewById(R.id.showOutput);
        outputView.setText("Requesting...");
        JSONObject parameters = new JSONObject();

        try {
            parameters.put("cleaning_parameter_set", 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RobartSdk.actionClean(this, parameters,
                new RobartSdk.SuccessCallback() {
                    @Override
                    public void onSuccess(String result) {
                        outputView.setText(result);
                    }
                },
                new RobartSdk.ErrorCallback() {
                    @Override
                    public void onError(String result) {
                        outputView.setText(result);
                    }
                },
                null, null
        );

    }

    /**
     * Implementation of robotCheckStatus SDK method, for more info read the SDK documentation
     *
     * @param View Sample View
     * @return Void
     */
    public void robotCheckStatus(View View) {
        final TextView outputView = (TextView) findViewById(R.id.showOutput);
        outputView.setText("Requesting...");
        JSONObject parameters = new JSONObject();

        RobartSdk.robotCheckStatus(this, parameters,
                new RobartSdk.SuccessCallback() {
                    @Override
                    public void onSuccess(String result) {
                        outputView.setText(result);
                    }
                },
                new RobartSdk.ErrorCallback() {
                    @Override
                    public void onError(String result) {
                        outputView.setText(result);
                    }
                },
                null, null
        );

    }

    /**
     * Implementation of scheduleEnable SDK method, for more info read the SDK documentation
     * @param View Sample View
     * @return Void
     */
    public void scheduleEnable(View View) {
        final TextView outputView = (TextView) findViewById(R.id.showOutput);
        int task_id = 1;
        RobartSdk.scheduleEnable(this, task_id,
                new RobartSdk.SuccessCallback(){
                    @Override
                    public void onSuccess(String result){
                        outputView.setText(result);
                    }
                },
                new RobartSdk.ErrorCallback(){
                    @Override
                    public void onError(String result){
                        outputView.setText(result);
                    }
                }
        );
    }

    /**
     * Implementation of scheduleDisable SDK method, for more info read the SDK documentation
     * @param View Sample View
     * @return Void
     */
    public void scheduleDisable(View View) {
        final TextView outputView = (TextView) findViewById(R.id.showOutput);
        int task_id = 1;
        RobartSdk.scheduleDisable(this, task_id,
                new RobartSdk.SuccessCallback(){
                    @Override
                    public void onSuccess(String result){
                        outputView.setText(result);
                    }
                },
                new RobartSdk.ErrorCallback(){
                    @Override
                    public void onError(String result){
                        outputView.setText(result);
                    }
                }
        );
    }

    /**
     * Implementation of scheduleList SDK method, for more info read the SDK documentation
     *
     * @param View Sample View
     * @return Void
     */
    public void scheduleList(View View) {
        final TextView outputView = (TextView) findViewById(R.id.showOutput);
        outputView.setText("Requesting...");
        JSONObject parameters = new JSONObject();

        RobartSdk.scheduleList(this, parameters,
                new RobartSdk.SuccessCallback() {
                    @Override
                    public void onSuccess(String result) {
                        outputView.setText(result);
                    }
                },
                new RobartSdk.ErrorCallback() {
                    @Override
                    public void onError(String result) {
                        outputView.setText(result);
                    }
                },
                null, null
        );

    }

    /**
     * Implementation of scheduleAdd SDK method, for more info read the SDK documentation
     *
     * @param View Sample View
     * @return Void
     */
    public void scheduleAdd(View View) {
        final TextView outputView = (TextView) findViewById(R.id.showOutput);
        outputView.setText("Requesting...");
        JSONObject parameters = new JSONObject();

        try {
            parameters.put("cleaning_mode", 1);
            parameters.put("cleaning_parameter_set", 1);
            parameters.put("days_of_week", 1);
            parameters.put("hour", 0);
            parameters.put("min", 0);
            parameters.put("map_id", 1);
            parameters.put("param1", 0);
            parameters.put("param2", 0);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RobartSdk.scheduleAdd(this, parameters,
                new RobartSdk.SuccessCallback() {
                    @Override
                    public void onSuccess(String result) {
                        outputView.setText(result);
                    }
                },
                new RobartSdk.ErrorCallback() {
                    @Override
                    public void onError(String result) {
                        outputView.setText(result);
                    }
                },
                null, null
        );

    }

    /**
     * Implementation of scheduleDelete SDK method, for more info read the SDK documentation
     *
     * @param View Sample View
     * @return Void
     */
    public void scheduleDelete(View View) {
        final TextView outputView = (TextView) findViewById(R.id.showOutput);
        outputView.setText("Requesting...");
        JSONObject parameters = new JSONObject();

        try {
            parameters.put("task_id", 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RobartSdk.scheduleDelete(this, parameters,
                new RobartSdk.SuccessCallback() {
                    @Override
                    public void onSuccess(String result) {
                        outputView.setText(result);
                    }
                },
                new RobartSdk.ErrorCallback() {
                    @Override
                    public void onError(String result) {
                        outputView.setText(result);
                    }
                },
                null, null
        );

    }

    /**
     * Implementation of scheduleEdit SDK method, for more info read the SDK documentation
     *
     * @param View Sample View
     * @return Void
     */
    public void scheduleEdit(View View) {
        final TextView outputView = (TextView) findViewById(R.id.showOutput);
        outputView.setText("Requesting...");
        JSONObject parameters = new JSONObject();

        try {
            parameters.put("task_id", 1);
            parameters.put("cleaning_mode", 1);
            parameters.put("cleaning_parameter_set", 1);
            parameters.put("days_of_week", 1);
            parameters.put("hour", 0);
            parameters.put("min", 0);
            parameters.put("map_id", 1);
            parameters.put("param1", 0);
            parameters.put("param2", 0);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RobartSdk.scheduleEdit(this, parameters,
                new RobartSdk.SuccessCallback() {
                    @Override
                    public void onSuccess(String result) {
                        outputView.setText(result);
                    }
                },
                new RobartSdk.ErrorCallback() {
                    @Override
                    public void onError(String result) {
                        outputView.setText(result);
                    }
                },
                null, null
        );

    }

    /**
     * Implementation of robotConfigureWifi SDK method, for more info read the SDK documentation
     *
     * @param View Sample View
     * @return Void
     */
    public void robotConfigureWifi(View View) {
        final TextView outputView = (TextView) findViewById(R.id.showOutput);
        outputView.setText("Requesting...");
        JSONObject parameters = new JSONObject();

        try {
            parameters.put("ssid", "sample");
            parameters.put("passphrase", "sample");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RobartSdk.robotConfigureWifi(this, parameters,
                new RobartSdk.SuccessCallback() {
                    @Override
                    public void onSuccess(String result) {
                        outputView.setText(result);
                    }
                },
                new RobartSdk.ErrorCallback() {
                    @Override
                    public void onError(String result) {
                        outputView.setText(result);
                    }
                },
                null, null
        );

    }

    /**
     * Implementation of actionAddSpotForCleaning SDK method, for more info read the SDK documentation
     *
     * @param View Sample View
     * @return Void
     */
    public void actionAddSpotForCleaning(View View) {
        final TextView outputView = (TextView) findViewById(R.id.showOutput);
        outputView.setText("Requesting...");
        JSONObject parameters = new JSONObject();

        try {
            parameters.put("map_id", 1);
            parameters.put("x1", 1.28);
            /*parameters.put("y1", 2.89);*/
            parameters.put("cleaning_parameter_set", 0);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RobartSdk.actionAddSpotForCleaning(this, parameters,
                new RobartSdk.SuccessCallback() {
                    @Override
                    public void onSuccess(String result) {
                        outputView.setText(result);
                    }
                },
                new RobartSdk.ErrorCallback() {
                    @Override
                    public void onError(String result) {
                        outputView.setText(result);
                    }
                },
                null, null
        );

    }

    /**
     * Implementation of actionSetSuctionControl SDK method, for more info read the SDK documentation
     *
     * @param View Sample View
     * @return Void
     */
    public void actionSetSuctionControl(View View) {
        final TextView outputView = (TextView) findViewById(R.id.showOutput);
        outputView.setText("Requesting...");
        JSONObject parameters = new JSONObject();

        try {
            parameters.put("cleaning_parameter_set", 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RobartSdk.actionSetSuctionControl(this, parameters,
                new RobartSdk.SuccessCallback() {
                    @Override
                    public void onSuccess(String result) {
                        outputView.setText(result);
                    }
                },
                new RobartSdk.ErrorCallback() {
                    @Override
                    public void onError(String result) {
                        outputView.setText(result);
                    }
                },
                null, null
        );

    }

    /**
     * Implementation of robotSetTime SDK method, for more info read the SDK documentation
     *
     * @param View Sample View
     * @return Void
     */
    public void robotSetTime(View View) {
        final TextView outputView = (TextView) findViewById(R.id.showOutput);
        outputView.setText("Requesting...");
        JSONObject parameters = new JSONObject();

        try {
            parameters.put("year", 2016);
            parameters.put("month", 6);
            parameters.put("day", 1);
            parameters.put("hour", 16);
            parameters.put("min", 9);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RobartSdk.robotSetTime(this, parameters,
                new RobartSdk.SuccessCallback() {
                    @Override
                    public void onSuccess(String result) {
                        outputView.setText(result);
                    }
                },
                new RobartSdk.ErrorCallback() {
                    @Override
                    public void onError(String result) {
                        outputView.setText(result);
                    }
                },
                null, null
        );

    }

    /**
     * Implementation of robotGetEventList SDK method, for more info read the SDK documentation
     *
     * @param View Sample View
     * @return Void
     */
    public void robotGetEventList(View View) {
        final TextView outputView = (TextView) findViewById(R.id.showOutput);
        outputView.setText("Requesting...");
        JSONObject parameters = new JSONObject();
        try {
            parameters.put("last_id", 0);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RobartSdk.robotGetEventList(this, parameters,
                new RobartSdk.SuccessCallback() {
                    @Override
                    public void onSuccess(String result) {
                        outputView.setText(result);
                    }
                },
                new RobartSdk.ErrorCallback() {
                    @Override
                    public void onError(String result) {
                        outputView.setText(result);
                    }
                },
                null, null
        );

    }

    /**
     * Implementation of robotScanWifiNetworks SDK method, for more info read the SDK documentation
     *
     * @param View Sample View
     * @return Void
     */
    public void robotScanWifiNetworks(View View) {
        final TextView outputView = (TextView) findViewById(R.id.showOutput);
        outputView.setText("Requesting...");
        JSONObject parameters = new JSONObject();

        RobartSdk.robotScanWifiNetworks(this,
                new RobartSdk.SuccessCallback() {
                    @Override
                    public void onSuccess(String result) {
                        outputView.setText(result);
                    }
                },
                new RobartSdk.ErrorCallback() {
                    @Override
                    public void onError(String result) {
                        outputView.setText(result);
                    }
                },
                null, null
        );

    }

    /**
     * Implementation of robotGetWifiList SDK method, for more info read the SDK documentation
     *
     * @param View Sample View
     * @return Void
     */
    public void robotGetWifiList(View View) {
        final TextView outputView = (TextView) findViewById(R.id.showOutput);
        outputView.setText("Requesting...");

        RobartSdk.robotGetWifiList(this,
                new RobartSdk.SuccessCallback() {
                    @Override
                    public void onSuccess(String result) {
                        outputView.setText(result);
                    }
                },
                new RobartSdk.ErrorCallback() {
                    @Override
                    public void onError(String result) {
                        outputView.setText(result);
                    }
                },
                null, null
        );

    }

    /**
     * Implementation of mapGetRobotPosition SDK method, for more info read the SDK documentation
     *
     * @param View Sample View
     * @return Void
     */
    public void mapGetRobotPosition(View View) {
        final TextView outputView = (TextView) findViewById(R.id.showOutput);
        outputView.setText("Requesting...");

        RobartSdk.mapGetRobotPosition(this,
                new RobartSdk.SuccessCallback() {
                    @Override
                    public void onSuccess(String result) {
                        outputView.setText(result);
                    }
                },
                new RobartSdk.ErrorCallback() {
                    @Override
                    public void onError(String result) {
                        outputView.setText(result);
                    }
                },
                null, null
        );

    }

    /**
     * Implementation of mapGetMovableObjectsList SDK method, for more info read the SDK documentation
     *
     * @param View Sample View
     * @return Void
     */
    public void mapGetMovableObjectsList(View View) {
        final TextView outputView = (TextView) findViewById(R.id.showOutput);
        outputView.setText("Requesting...");

        RobartSdk.mapGetMovableObjectsList(this,
                new RobartSdk.SuccessCallback() {
                    @Override
                    public void onSuccess(String result) {
                        outputView.setText(result);
                    }
                },
                new RobartSdk.ErrorCallback() {
                    @Override
                    public void onError(String result) {
                        outputView.setText(result);
                    }
                },
                null, null
        );

    }

    /**
     * Implementation of mapGetRoomLayout SDK method, for more info read the SDK documentation
     *
     * @param View Sample View
     * @return Void
     */
    public void mapGetRoomLayout(View View) {
        final TextView outputView = (TextView) findViewById(R.id.showOutput);
        outputView.setText("Requesting...");
        RobartSdk.mapGetRoomLayout(this,
                new RobartSdk.SuccessCallback() {
                    @Override
                    public void onSuccess(String result) {
                        outputView.setText(result);
                    }
                },
                new RobartSdk.ErrorCallback() {
                    @Override
                    public void onError(String result) {
                        outputView.setText(result);
                    }
                },
                null, null
        );

    }

    /**
     * Implementation of mapGetCleaningGrid SDK method, for more info read the SDK documentation
     *
     * @param View Sample View
     * @return Void
     */
    public void mapGetCleaningGrid(View View) {
        final TextView outputView = (TextView) findViewById(R.id.showOutput);
        outputView.setText("Requesting...");

        RobartSdk.mapGetCleaningGrid(this,
                new RobartSdk.SuccessCallback() {
                    @Override
                    public void onSuccess(String result) {
                        outputView.setText(result);
                    }
                },
                new RobartSdk.ErrorCallback() {
                    @Override
                    public void onError(String result) {
                        outputView.setText(result);
                    }
                },
                null, null
        );
    }

    /**
     * Implementation of robotGetVersion SDK method, for more info read the SDK documentation
     *
     * @param View Sample View
     * @return Void
     */
    public void robotGetVersion(View View) {
        final TextView outputView = (TextView) findViewById(R.id.showOutput);
        outputView.setText("Checking version...");
        JSONObject parameters = new JSONObject();
        RobartSdk.robotGetVersion(this, parameters,
                new RobartSdk.SuccessCallback() {
                    @Override
                    public void onSuccess(String result) {
                        outputView.setText(result);
                    }
                },
                new RobartSdk.ErrorCallback() {
                    @Override
                    public void onError(String result) {
                        outputView.setText(result);
                    }
                },
                null
        );
    }


    /**
     * Implementation of robotGetMacAddress SDK method, for more info read the SDK documentation
     * @param View Sample View
     * @return Void
     */
    public void robotGetMacAddress(View View) {
        final TextView outputView = (TextView) findViewById(R.id.showOutput);
        outputView.setText("Requesting...");

        RobartSdk.robotGetMacAddress(this,
                new RobartSdk.SuccessCallback(){
                    @Override
                    public void onSuccess(String result){
                        outputView.setText(result);
                    }
                },
                new RobartSdk.ErrorCallback(){
                    @Override
                    public void onError(String result){
                        outputView.setText(result);
                    }
                }
        );

    };

    /**
     * Implementation of sendBugReport SDK method, for more info read the SDK documentation
     * @param View Sample View
     * @return Void
     */
    public void sendBugReport(View View) {
        final TextView outputView = (TextView) findViewById(R.id.showOutput);
        outputView.setText("Sending mail...");
        RobartSdk.sendBugReport(this);

    };



}

/**
 * Copyright (c) 2016 Robart
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including the rights to use, copy, modify,
 * merge, publish, distribute, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @author  Plumeria Solutions
 * @version 1.3
 * @since   2016-07-05
 */

package com.vacuum.robart.robartsdk;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.util.Base64;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Random;
import java.util.regex.Pattern;

/**
 * The RobartSdk class provide and interface to define a communication between the mobile app and
 * the Robot, handling all the private date as and intermediate layer. It is managed by callbacks
 * so every method of the class will execute a provided success or error callback once the task is
 * completed.
 */
public class RobartSdk {
    private static String robotEndpoint = "";
    private static String firmwareURL = "http://update.robart.cc:8080/";
    private static String iOTEndpoint = "https://52.57.14.46/iot/";
    private static String PLTSK = null;
    private static String USER_ID = null;
    private static String currentFirmware = null;
    private static String lastUniqueId = null;
    private static String lastToken = null;


/*    public static void initializeDiscoveryListener(Context context) {
        final NsdManager mNsdManager;
        NsdManager.DiscoveryListener mDiscoveryListener;
        NsdManager.ResolveListener mResolveListener = new NsdManager.ResolveListener() {
            @Override
            public void onResolveFailed(NsdServiceInfo serviceInfo, int errorCode) {
                // Called when the resolve fails.  Use the error code to debug.
                System.out.println("Resolve failed" + errorCode);
            }
            @Override
            public void onServiceResolved(NsdServiceInfo serviceInfo) {
                System.out.println("Resolve Succeeded. " + serviceInfo);
                int port = serviceInfo.getPort();
                InetAddress host = serviceInfo.getHost();
                System.out.println("--------------------------->>>");
                System.out.println(port);
                System.out.println(host);
            }
        };

        //final String SERVICE_TYPE = "_aicu-http._tcp.local.";
        final String SERVICE_TYPE = "_aicu-http._tcp";

        mNsdManager = (NsdManager) context.getSystemService(Context.NSD_SERVICE);

        // Instantiate a new DiscoveryListener
        mDiscoveryListener = new NsdManager.DiscoveryListener() {

            @Override
            public void onDiscoveryStarted(String regType) {
                System.out.println("Service discovery started");
            }
            @Override
            public void onServiceFound(NsdServiceInfo service) {
                System.out.println("Service discovery success" + service);
                if (!service.getServiceType().equals(SERVICE_TYPE)) {
                    System.out.println("Unknown Service Type: " + service.getServiceType());
                }
                System.out.println(service.getPort());
                System.out.println(service.getHost());
            }

            @Override
            public void onServiceLost(NsdServiceInfo service) {
                System.out.println("service lost" + service);
            }

            @Override
            public void onDiscoveryStopped(String serviceType) {
                System.out.println("Discovery stopped: " + serviceType);
            }

            @Override
            public void onStartDiscoveryFailed(String serviceType, int errorCode) {
                System.out.println("Discovery failed: Error code:" + errorCode);
                mNsdManager.stopServiceDiscovery(this);
            }

            @Override
            public void onStopDiscoveryFailed(String serviceType, int errorCode) {
                System.out.println("Discovery failed: Error code:" + errorCode);
                mNsdManager.stopServiceDiscovery(this);
            }
        };
        mNsdManager.discoverServices(SERVICE_TYPE, NsdManager.PROTOCOL_DNS_SD, mDiscoveryListener);
    }
    public static void zeroConf(Context context, final SuccessCallback callback, final ErrorCallback failure) {
        initializeDiscoveryListener(context);
    };*/

    /**
     * Returns a random integer between the range.
     * @param min Min random range.
     * @param max Max random range
     * @return int
     */
    private static int randomInt(int min, int max, List<Integer> exclude) {
        Random rand = new Random();
        int randomNumber;
        boolean includedInEx = false;
        if(exclude == null){
            exclude = new ArrayList<>();
        }
        if(exclude.size() >= max-min){
            throw new IllegalArgumentException("List of excluded values equals or exceeds range!");
        }
        do{
            randomNumber = min + rand.nextInt(max - min + 1);
        }while (exclude.indexOf(randomNumber) > -1 );

        return randomNumber;
    }

    /**
     * Generates the PLTSK
     * @return String
     */
    private static String generatePLTSK () {
        int TOTAL_LENGTH = randomInt(32, 64, null);
        String RANDOM_STRING = "";
        final char[] ALPHA_UPPER_CHARACTERS = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q','R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
        final char[] ALPHA_LOWER_CHARACTERS = { 'a', 'b', 'c', 'd','e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q','r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };
        final char[] NUMERIC_CHARACTERS = { '0', '1', '2', '3', '4','5', '6', '7', '8', '9' };
        final char[] SPECIAL_CHARACTERS = { '+', '/' , '=' };
        int CHARSET_TYPE;
        int CHARSET_LENGTH;
        String LAST_STRING;
        String REPEATED_THREE_TIMES;
        for (int i = 0; i < TOTAL_LENGTH; i++) {
            //SELECTS RANDOM A TYPE OF CHARSET
            CHARSET_TYPE = randomInt(1,4, null);
            //VALIDATIONS ON CONSTRUCTION
            if(RANDOM_STRING.length() >= 2 && Pattern.matches("[0-9]{2}", RANDOM_STRING.substring(RANDOM_STRING.length() - 2))) {
                List<Integer> FORBIDDEN = new ArrayList<Integer>();
                FORBIDDEN.add(3);
                CHARSET_TYPE = randomInt(1, 4, FORBIDDEN);
            }
            if(RANDOM_STRING.length() >= 2 && Pattern.matches("[/+=]{2}", RANDOM_STRING.substring(RANDOM_STRING.length() - 2))) {
                CHARSET_TYPE = randomInt(1, 3, null);
            }
            if(RANDOM_STRING.length() >= 4 && Pattern.matches("[a-zA-Z]{4}", RANDOM_STRING.substring(RANDOM_STRING.length() - 4))) {
                CHARSET_TYPE = randomInt(3, 4, null);
            }
            if(RANDOM_STRING.length() >= 5 && Pattern.matches("qwert", RANDOM_STRING.substring(RANDOM_STRING.length() - 5))) {
                CHARSET_TYPE = randomInt(3, 4, null);
            }
            if(RANDOM_STRING.length() >= 3) {
                LAST_STRING = RANDOM_STRING.substring(RANDOM_STRING.length() - 1);
                REPEATED_THREE_TIMES = String.format("[%s]{4}",LAST_STRING);
                if (Pattern.matches(REPEATED_THREE_TIMES, RANDOM_STRING.substring(RANDOM_STRING.length() - 3))) {
                    if (Pattern.matches("[a-zA-Z]{1}", LAST_STRING)) {
                        CHARSET_TYPE = randomInt(3, 4, null);
                    } else if (Pattern.matches("[0-9]{1}", LAST_STRING)) {
                        List<Integer> FORBIDDEN = new ArrayList<Integer>();
                        FORBIDDEN.add(3);
                        CHARSET_TYPE = randomInt(1, 4, FORBIDDEN);
                    } else {
                        CHARSET_TYPE = randomInt(1, 3, null);
                    }
                }
            }
            //WHIT A CHARSET TYPE SELECTS A RANDOM CHARACTER OF THE CHARSET, GET THE CHARSET LENGTH AS MAX AND THEN GETS A CHARACTER AND APPEND IT TO THE FINAL STRING
            switch (CHARSET_TYPE) {
                case 1:
                    CHARSET_LENGTH = ALPHA_UPPER_CHARACTERS.length - 1;
                    RANDOM_STRING += ALPHA_UPPER_CHARACTERS[randomInt(0, CHARSET_LENGTH, null)];
                    break;
                case 2:
                    CHARSET_LENGTH = ALPHA_LOWER_CHARACTERS.length - 1;
                    RANDOM_STRING += ALPHA_LOWER_CHARACTERS[randomInt(0, CHARSET_LENGTH, null)];
                    break;
                case 3:
                    CHARSET_LENGTH = NUMERIC_CHARACTERS.length - 1;
                    RANDOM_STRING += NUMERIC_CHARACTERS[randomInt(0, CHARSET_LENGTH, null)];
                    break;
                case 4:
                    CHARSET_LENGTH = SPECIAL_CHARACTERS.length - 1;
                    RANDOM_STRING += SPECIAL_CHARACTERS[randomInt(0, CHARSET_LENGTH, null)];
                    break;
            }
        }
        //IF IT DOES NOT HAVE AT LEAST 1 SPECIAL CHAR, UPPER, LOWER OR NUMBER, GENERATES ONE AND SETS AT RANDOM POSITION
        List<Integer> FORBIDDEN_NUMBERS = new ArrayList<Integer>();

        if (!Pattern.matches("^(?=.*[/+=]).*$", RANDOM_STRING)) {
            int RANDOM_POS_TO_REPLACE = randomInt(10,RANDOM_STRING.length() - 1, null);
            FORBIDDEN_NUMBERS.add(RANDOM_POS_TO_REPLACE);
            StringBuilder TEMPORAL_STRING = new StringBuilder(RANDOM_STRING);
            CHARSET_LENGTH = SPECIAL_CHARACTERS.length - 1;
            TEMPORAL_STRING.setCharAt(RANDOM_POS_TO_REPLACE, SPECIAL_CHARACTERS[randomInt(0, CHARSET_LENGTH, null)]);
            RANDOM_STRING = TEMPORAL_STRING.toString();
        }
        if (!Pattern.matches("^(?=.*[A-Z]).*$", RANDOM_STRING)) {
            int RANDOM_POS_TO_REPLACE = randomInt(10,RANDOM_STRING.length() - 1, FORBIDDEN_NUMBERS);
            FORBIDDEN_NUMBERS.add(RANDOM_POS_TO_REPLACE);
            StringBuilder TEMPORAL_STRING = new StringBuilder(RANDOM_STRING);
            CHARSET_LENGTH = ALPHA_UPPER_CHARACTERS.length - 1;
            TEMPORAL_STRING.setCharAt(RANDOM_POS_TO_REPLACE, ALPHA_UPPER_CHARACTERS[randomInt(0, CHARSET_LENGTH, null)]);
            RANDOM_STRING = TEMPORAL_STRING.toString();
        }
        if (!Pattern.matches("^(?=.*[a-z]).*$", RANDOM_STRING)) {
            int RANDOM_POS_TO_REPLACE = randomInt(10,RANDOM_STRING.length() - 1, FORBIDDEN_NUMBERS);
            FORBIDDEN_NUMBERS.add(RANDOM_POS_TO_REPLACE);
            StringBuilder TEMPORAL_STRING = new StringBuilder(RANDOM_STRING);
            CHARSET_LENGTH = ALPHA_LOWER_CHARACTERS.length - 1;
            TEMPORAL_STRING.setCharAt(RANDOM_POS_TO_REPLACE, ALPHA_LOWER_CHARACTERS[randomInt(0, CHARSET_LENGTH, null)]);
            RANDOM_STRING = TEMPORAL_STRING.toString();
        }
        if (!Pattern.matches("^(?=.*[0-9]).*$", RANDOM_STRING)) {
            int RANDOM_POS_TO_REPLACE = randomInt(10,RANDOM_STRING.length() - 1, FORBIDDEN_NUMBERS);
            StringBuilder TEMPORAL_STRING = new StringBuilder(RANDOM_STRING);
            CHARSET_LENGTH = NUMERIC_CHARACTERS.length - 1;
            TEMPORAL_STRING.setCharAt(RANDOM_POS_TO_REPLACE, NUMERIC_CHARACTERS[randomInt(0, CHARSET_LENGTH, null)]);
            RANDOM_STRING = TEMPORAL_STRING.toString();
        }

        System.out.println(RANDOM_STRING);
        return RANDOM_STRING;
    };

    /**
     * Test of the generatePLTSK() method;
     * @return String
     */
    public static String testPLTSK () {
        return generatePLTSK();
    };

    /**
     * Gets the PLTSK if exist, if not it generates it: The generation uses the UUID of
     * the Android device, and then it is encrypted with
     * SHA1 algorithm.
     * @return String
     */
    private static String getPLTSK() {
        if(PLTSK != null) {
            return PLTSK;
        }
        PLTSK = generatePLTSK();
        return  PLTSK;
    };

    /**
     * Executes the Auth request to the IOT server with the current user_id, stsk and PLTSK.
     * @param context The current context
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @return Void
     */
    private static void auth(Context context, String user_id, String stsk, final SuccessCallback callback, final ErrorCallback failure) {
        JSONObject parameters = new JSONObject();
        try {
            parameters.put("user_id", user_id);
            parameters.put("stsk", stsk);
            parameters.put("pltsk", getPLTSK());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        GET(context, parameters, "iot/auth", null, callback, failure, user_id, null);
    };

    /**
     * Links a device to the user user.
     * @param context The current context
     * @param user_id User unique id created when registering to b2b server.
     * @param robot_id Robot unique id received when pairing locally.
     * @param robot_password Robot password configured by user.
     * @param stsk Shared temporary session key (STSK) Token send by b2b server to be used in authentication process.
     * @param failure Callback to be executed on failure
     * @return Void
     */
    private static void addDevice(Context context, String user_id, String robot_id, String robot_password, String stsk, final SuccessCallback callback, final ErrorCallback failure) {
        System.out.println("Executing addDevice");
        JSONObject parameters = new JSONObject();
        try {
            parameters.put("user_id", user_id);
            parameters.put("robot_id", robot_id);
            parameters.put("robot_password", robot_password);
            parameters.put("stsk", stsk);
            parameters.put("pltsk", getPLTSK());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        GET(context, parameters, "iot/add_device", null, callback, failure, user_id, robot_id);
    };

    /**
     * @param context The current context
     * Links a robot to the provided user
     * @param robot_id Robot unique id received when pairing locally.
     * @param robot_password Robot password configured by user.
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @return Void
     */
    private static void addRobot(Context context, String robot_id, String robot_password, String user_id, final SuccessCallback callback, final ErrorCallback failure) {
        System.out.println("Executing addRobot");
        JSONObject parameters = new JSONObject();
        try {
            parameters.put("robot_id", robot_id);
            parameters.put("robot_password", robot_password);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        GET(context, parameters, "iot/add_robot", null, callback, failure, user_id, robot_id);
    };

    /**
     * Gets linked robots from the iot server.
     * @param context The current context
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @return Void
     */
    private static void getRobots(Context context, SuccessCallback callback, ErrorCallback failure) {
        System.out.println("Executing getRobots");
        JSONObject parameters = new JSONObject();
        GET(context, parameters, "iot/get_robots", null, callback, failure, USER_ID, null);
    };

    /**
     * Starts the initializeSDKIOTConnection method
     * @param context The current context
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     */
    public static void initializeSDKIOTConnection(final Context context, final SuccessCallback callback, final ErrorCallback failure) {
        RobartSdk.SuccessCallback successCallback = new RobartSdk.SuccessCallback(){
            @Override
            public void onSuccess(String result){
                JSONObject response = new JSONObject();
                try {
                    response.put("message", "OK");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                callback.onSuccess(response.toString());
            }
        };
        if (PLTSK == null || USER_ID == null) {
            JSONObject response = new JSONObject();
            try {
                response.put("error", "IOT Configuration not done yet");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            failure.onError(response.toString());
        } else {
            getRobots(context, successCallback, failure);
        }
    };

    /**
     * Registers the phone on the IOT server.
     * the IOT server.
     * @param context The current context
     * @param user_id User unique id created when registering to b2b server.
     * @param stsk Shared temporary session key (STSK) Token sent by b2b server to be used in authentication process.
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     */
    public static void registerIOT(final Context context, final String user_id, final String stsk, final String robot_id, final String robot_password, final SuccessCallback callback, final ErrorCallback failure) {

        final RobartSdk.SuccessCallback onCompleted = new RobartSdk.SuccessCallback(){
            @Override
            public void onSuccess(String result){
                USER_ID = user_id;
                callback.onSuccess(result);
            }
        };

        RobartSdk.SuccessCallback successCallback = new RobartSdk.SuccessCallback(){
            @Override
            public void onSuccess(String result){
                addRobot(context, robot_id, robot_password, user_id, onCompleted, failure);
            }
        };

        RobartSdk.ErrorCallback errorCallback = new RobartSdk.ErrorCallback(){
            @Override
            public void onError(String result){
                if(result.equals("Confirmation required")) {
                    addDevice(context, user_id,robot_id, robot_password, stsk, onCompleted, failure);
                } else {
                    failure.onError(result);
                }
            }
        };
        auth(context, user_id, stsk, successCallback, errorCallback);
    };

    /**
     * Selects the robot at the given url and port endpoint, always required to intereact with
     * the robot.
     * @param endpoint The url and port of the Robot.
     * @return void
     */
    public static boolean robotSelect(String endpoint) {
        String last_character = endpoint.substring(endpoint.length() - 1);
        String slash_character = "/";
        if (!last_character.equals(slash_character)) {
            return false;
        }
        robotEndpoint = endpoint;
        return true;
    };

    /**
     * Returns a Callback resolving the response from the robot status.
     * @param context The current context
     * @param parameters Json object with parameters, refer to documentation for details.
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @param user_id User unique id created when registering to b2b server. Only mandatory when using an IOT connection to the robot.
     * @param robot_id Available via robotGetRobotId. Only mandatory when using an IOT connection to the robot.
     */
    public static void robotGetRobotStatus(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure, String user_id, String robot_id) {
        GET(context, parameters, "get/status", null, callback, failure, user_id, robot_id);
    };
    /**
     * Returns a Callback resolving the response from the robot status.
     * @param context The current context
     * @param parameters Json object with parameters, refer to documentation for details.
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     */
    public static void robotGetRobotStatus(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure) {
        robotGetRobotStatus(context, parameters, callback, failure, null, null);
    };

    /**
     * Returns a Callback containing the robot name.
     * @param context The current context
     * @param parameters Json object with parameters, refer to documentation for details.
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @param user_id User unique id created when registering to b2b server. Only mandatory when using an IOT connection to the robot.
     * @param robot_id Available via robotGetRobotId. Only mandatory when using an IOT connection to the robot.
     * @return void
     */
    public static void robotGetRobotName(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure, String user_id, String robot_id) {

        robotGetRobotId(context, parameters,
                new RobartSdk.SuccessCallback(){
                    @Override
                    public void onSuccess(String result){
                        String name = null;
                        JSONObject response = new JSONObject();
                        try {
                            JSONObject resultJSON = new JSONObject(result);
                            name = resultJSON.getString("name");
                            response.put("name", name);
                            callback.onSuccess(response.toString());
                        } catch (JSONException e) {
                            failure.onError(e.toString());
                        }
                    }
                },
                new RobartSdk.ErrorCallback(){
                    @Override
                    public void onError(String result){
                        failure.onError(result);
                    }
                },
                user_id, robot_id
        );

    };

    /**
     * Returns a Callback containing the robot name.
     * @param context The current context
     * @param parameters Json object with parameters, refer to documentation for details.
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     */
    public static void robotGetRobotName(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure) {
        robotGetRobotName(context, parameters, callback, failure, null, null);
    }


    /**
     * Returns an object containing the robot information (name, unique_id,
     * model, firmware).
     * @param context The current context
     * @param parameters Json object with parameters, refer to documentation for details.
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @param user_id User unique id created when registering to b2b server. Only mandatory when using an IOT connection to the robot.
     * @param robot_id Available via robotGetRobotId. Only mandatory when using an IOT connection to the robot.
     * @return void
     */
    public static void robotGetRobotId(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure, String user_id, String robot_id) {
        GET(context, parameters, "get/robot_id", null, callback, failure, user_id, robot_id);
    };

    /**
     * Returns an object containing the robot information (name, unique_id,
     * model, firmware).
     * @param context The current context
     * @param parameters Json object with parameters, refer to documentation for details.
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     */
    public static void robotGetRobotId(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure) {
        robotGetRobotId(context, parameters, callback, failure, null, null);
    };

    /**
     * Orders the robot to return to home.
     * @param context The current context
     * @param parameters Json object with parameters, refer to documentation for details.
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @param user_id User unique id created when registering to b2b server. Only mandatory when using an IOT connection to the robot.
     * @param robot_id Available via robotGetRobotId. Only mandatory when using an IOT connection to the robot.
     * @return void
     */
    public static void actionGoHome(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure, String user_id, String robot_id) {
        GET(context, parameters, "set/go_home", null, callback, failure, user_id, robot_id);
    };

    /**
     * Orders the robot to return to home.
     * @param context The current context
     * @param parameters Json object with parameters, refer to documentation for details.
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     */
    public static void actionGoHome(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure) {
        actionGoHome(context, parameters, callback, failure, null, null);
    };

    /**
     * Orders the robot to stop current on going action.
     * @param context The current context
     * @param parameters Json object with parameters, refer to documentation for details.
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @param user_id User unique id created when registering to b2b server. Only mandatory when using an IOT connection to the robot.
     * @param robot_id Available via robotGetRobotId. Only mandatory when using an IOT connection to the robot.
     * @return void
     */
    public static void actionStop(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure, String user_id, String robot_id) {
        GET(context, parameters, "set/stop", null, callback, failure, user_id, robot_id);
    };

    /**
     * Orders the robot to stop current on going action.
     * @param context The current context
     * @param parameters Json object with parameters, refer to documentation for details.
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     */
    public static void actionStop(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure) {
        actionStop(context, parameters, callback, failure, null, null);
    };



    /**
     * Orders the robot to start cleaning.
     * @param context The current context
     * @param parameters Json object with parameters, refer to documentation for details.
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @param user_id User unique id created when registering to b2b server. Only mandatory when using an IOT connection to the robot.
     * @param robot_id Available via robotGetRobotId. Only mandatory when using an IOT connection to the robot.
     * @return void
     */
    public static void actionClean(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure, String user_id, String robot_id) {
        //GET(context, parameters, "set/clean_start_or_continue?cleaning_parameter_set=0", null, callback, failure,user_id, robot_id);
        if(parameters.length() == 0){
            try {
                parameters.put("cleaning_parameter_set", 0);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        GET(context, parameters, "set/clean_start_or_continue", null, callback, failure,user_id, robot_id);
    }

    /**
     * Overridden method of actionClean command with no additional user_id and robot_id necessary
     * @param context
     * @param parameters
     * @param callback
     * @param failure
     */
    public static void actionClean(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure) {
        actionClean(context, parameters, callback, failure, null, null);
    }



    /**
     * checks the status of the current on going action by the robot.
     * @param context The current context
     * @param parameters Json object with parameters, refer to documentation for details.
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @param user_id User unique id created when registering to b2b server. Only mandatory when using an IOT connection to the robot.
     * @param robot_id Available via robotGetRobotId. Only mandatory when using an IOT connection to the robot.
     * @return void
     */
    public static void robotCheckStatus(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure, String user_id, String robot_id) {
        GET(context, parameters, "get/command_result", null, callback, failure, user_id, robot_id);
    };

    /**
     * checks the status of the current on going action by the robot.
     * @param context The current context
     * @param parameters Json object with parameters, refer to documentation for details.
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     */
    public static void robotCheckStatus(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure) {
        robotCheckStatus(context, parameters, callback, failure, null, null);
    };



    /**
     *  When success, return an array, containing all shcedules stored in the robot.
     * @param context The current context
     * @param parameters Json object with parameters, refer to documentation for details.
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @param user_id User unique id created when registering to b2b server. Only mandatory when using an IOT connection to the robot.
     * @param robot_id Available via robotGetRobotId. Only mandatory when using an IOT connection to the robot.
     */
    public static void scheduleList(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure, String user_id, String robot_id) {
        GET(context, parameters, "get/schedule", null, callback, failure, user_id, robot_id);
    };

    /**
     *  When success, return an array, containing all shcedules stored in the robot.
     * @param context The current context
     * @param parameters Json object with parameters, refer to documentation for details.
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     */
    public static void scheduleList(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure) {
        scheduleList(context, parameters, callback, failure, null, null);
    };


    /**
     * Adds a new schedule to the robot shcedule list.
     * @param context The current context
     * @param parameters Json object with parameters, refer to documentation for details.
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @param user_id User unique id created when registering to b2b server. Only mandatory when using an IOT connection to the robot.
     * @param robot_id Available via robotGetRobotId. Only mandatory when using an IOT connection to the robot.
     * @return void
     */
    public static void scheduleAdd(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure, String user_id, String robot_id) {
        GET(context, parameters, "set/add_scheduled_task", null, callback, failure, user_id, robot_id);
    };

    /**
     * Adds a new schedule to the robot shcedule list.
     * @param context The current context
     * @param parameters Json object with parameters, refer to documentation for details.
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     */
    public static void scheduleAdd(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure) {
        scheduleAdd(context, parameters, callback, failure, null, null);
    };


    /**
     * Deletes the given task_id from the robot schedule list.
     * @param context The current context
     * @param parameters Json object with parameters, refer to documentation for details.
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @param user_id User unique id created when registering to b2b server. Only mandatory when using an IOT connection to the robot.
     * @param robot_id Available via robotGetRobotId. Only mandatory when using an IOT connection to the robot.
     * @return void
     */
    public static void scheduleDelete(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure, String user_id, String robot_id) {
        GET(context, parameters, "set/delete_scheduled_task", null, callback, failure, user_id, robot_id);
    };

    /**
     * Deletes the given task_id from the robot schedule list.
     * @param context The current context
     * @param parameters Json object with parameters, refer to documentation for details.
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     */
    public static void scheduleDelete(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure) {
        scheduleDelete(context, parameters, callback, failure, null, null);
    };

    /**
     * If exist, edits an existing shedule on list, matching the provided task_id, with the new
     * information sent to the robot.
     * @param context The current context
     * @param parameters Json object with parameters, refer to documentation for details.
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @param user_id User unique id created when registering to b2b server. Only mandatory when using an IOT connection to the robot.
     * @param robot_id Available via robotGetRobotId. Only mandatory when using an IOT connection to the robot.
     * @return void
     */
    public static void scheduleEdit(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure, String user_id, String robot_id) {
        GET(context, parameters, "set/modify_scheduled_task", null, callback, failure, user_id, robot_id);
    };

    /**
     * If exist, edits an existing shedule on list, matching the provided task_id, with the new
     * information sent to the robot.
     * @param context The current context
     * @param parameters Json object with parameters, refer to documentation for details.
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     */
    public static void scheduleEdit(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure) {
        scheduleEdit(context, parameters, callback, failure, null, null);
    };



    /**
     * Instructs the robot to connect to the wifi network with the given ssid and passphrase.
     * @param context The current context
     * @param parameters Json object with parameters, refer to documentation for details.
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @param user_id User unique id created when registering to b2b server. Only mandatory when using an IOT connection to the robot.
     * @param robot_id Available via robotGetRobotId. Only mandatory when using an IOT connection to the robot.
     * @return void
     */
    public static void robotConfigureWifi(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure, String user_id, String robot_id) {
        GET(context, parameters, "set/connect_wifi", null, callback, failure, user_id, robot_id);
    };

    /**
     * Adds a new clening spot with thep provided data on the robot cleaning map.
     * @param context The current context
     * @param parameters Json object with parameters, refer to documentation for details.
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @param user_id User unique id created when registering to b2b server. Only mandatory when using an IOT connection to the robot.
     * @param robot_id Available via robotGetRobotId. Only mandatory when using an IOT connection to the robot.
     * @return void
     */
    public static void actionAddSpotForCleaning(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure, String user_id, String robot_id) {
        ArrayList<String> toIntegerKeysList = new ArrayList<String>();
        boolean hasXorY = false;
        try {
            parameters.get("x1");
            toIntegerKeysList.add("x1");
            hasXorY = true;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            parameters.get("y1");
            toIntegerKeysList.add("y1");
            hasXorY = true;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(hasXorY) {
            parameters = convertToFloat(parameters, toIntegerKeysList);
        }
        GET(context, parameters, "set/clean_spot", null, callback, failure, user_id, robot_id);
    };

    /**
     * Adds a new clening spot with thep provided data on the robot cleaning map.
     * @param context The current context
     * @param parameters Json object with parameters, refer to documentation for details.
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     */
    public static void actionAddSpotForCleaning(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure) {
        actionAddSpotForCleaning(context, parameters, callback, failure, null, null);
    }

    /**
     * Configures the suction intensity of the robot for the current on going task.
     * @param context The current context
     * @param parameters Json object with parameters, refer to documentation for details.
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @param user_id User unique id created when registering to b2b server. Only mandatory when using an IOT connection to the robot.
     * @param robot_id Available via robotGetRobotId. Only mandatory when using an IOT connection to the robot.
     * @return void
     */
    public static void actionSetSuctionControl(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure, String user_id, String robot_id) {
        GET(context, parameters, "set/switch_cleaning_parameter_set", null, callback, failure, user_id, robot_id);
    };

    /**
     * Configures the suction intensity of the robot for the current on going task.
     * @param context The current context
     * @param parameters Json object with parameters, refer to documentation for details.
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     */
    public static void actionSetSuctionControl(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure) {
        actionSetSuctionControl(context, parameters, callback, failure, null, null);
    };



    /**
     * Sets the robot local time according to the provided data.
     * @param context The current context
     * @param parameters Json object with parameters, refer to documentation for details.
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @param user_id User unique id created when registering to b2b server. Only mandatory when using an IOT connection to the robot.
     * @param robot_id Available via robotGetRobotId. Only mandatory when using an IOT connection to the robot.
     * @return void
     */
    public static void robotSetTime(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure, String user_id, String robot_id) {
        GET(context, parameters, "set/time", null, callback, failure, user_id, robot_id);
    };

    /**
     * Sets the robot local time according to the provided data.
     * @param context The current context
     * @param parameters Json object with parameters, refer to documentation for details.
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     */
    public static void robotSetTime(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure) {
        robotSetTime(context, parameters, callback, failure, null, null);
    };

    /**
     * Return an array containing all events stored on the robot, a last_id is required to filter
     * the events and avoid returning them all always.
     * @param context The current context
     * @param parameters Json object with parameters, refer to documentation for details.
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @param user_id User unique id created when registering to b2b server. Only mandatory when using an IOT connection to the robot.
     * @param robot_id Available via robotGetRobotId. Only mandatory when using an IOT connection to the robot.
     */
    public static void robotGetEventList(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure, String user_id, String robot_id) {
        GET(context, parameters, "get/event_log", null, callback, failure, user_id, robot_id);
    };

    /**
     * Return an array containing all events stored on the robot, a last_id is required to filter
     * the events and avoid returning them all always.
     * @param context The current context
     * @param parameters Json object with parameters, refer to documentation for details.
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     */
    public static void robotGetEventList(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure) {
        robotGetEventList(context, parameters, callback, failure, null, null);
    };

    /**
     * Gives the robot the order to start the wifi scan.
     * @param context The current context
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     */
    public static void robotScanWifiNetworks(Context context, final SuccessCallback callback, final ErrorCallback failure, String user_id, String robot_id) {
        GET(context, null, "set/start_scan", null, callback, failure, user_id, robot_id);
    };

    /**
     * Gives the robot the order to start the wifi scan.
     * @param context The current context
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     */
    public static void robotScanWifiNetworks(Context context, final SuccessCallback callback, final ErrorCallback failure) {
        robotScanWifiNetworks(context, callback, failure, null, null);
    };

    /**
     *  Makes a request to the robot for the wifi connections detected in the wifi scan.
     * @param context The current context
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     */
    public static void robotGetWifiList(Context context, final SuccessCallback callback, final ErrorCallback failure, String user_id, String robot_id) {
        GET(context, null, "get/wifi_scan_results", null, callback, failure, user_id, robot_id);
    };

    /**
     *  Returns on success the robot position.
     * @param context The current context
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     */
    public static void robotGetWifiList(Context context, final SuccessCallback callback, final ErrorCallback failure) {
        robotGetWifiList(context, callback, failure, null, null);
    };

    /**
     *  Returns on success the robot position.
     * @param context The current context
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @param user_id User unique id created when registering to b2b server. Only mandatory when using an IOT connection to the robot.
     * @param robot_id Available via robotGetRobotId. Only mandatory when using an IOT connection to the robot.
     * @return void
     */
    public static void mapGetRobotPosition(Context context, final SuccessCallback callback, final ErrorCallback failure, String user_id, String robot_id) {
        GET(context, null, "get/rob_pose", null, callback, failure, user_id, robot_id);
    };

    /**
     *  Returns on success the robot position.
     * @param context The current context
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     */
    public static void mapGetRobotPosition(Context context, final SuccessCallback callback, final ErrorCallback failure) {
        mapGetRobotPosition(context, callback, failure, null, null);
    };

    /**
     * Returns on success an array with all polygons that are not considered appropriate to be used for the robots navigation.
     * If the optional parameter map_id is passed and the robot supports "permanent map" the requested data of the given map_id is returned.
     * @param context The current context
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @param user_id User unique id created when registering to b2b server. Only mandatory when using an IOT connection to the robot.
     * @param robot_id Available via robotGetRobotId. Only mandatory when using an IOT connection to the robot.
     * @return void
     */
    public static void mapGetMovableObjectsList(Context context, final SuccessCallback callback, final ErrorCallback failure, String user_id, String robot_id) {
        GET(context, null, "get/n_n_polygons", null, callback, failure, user_id, robot_id);
    };

    /**
     * Returns on success an array with all polygons that are not considered appropriate to be used for the robots navigation.
     * If the optional parameter map_id is passed and the robot supports "permanent map" the requested data of the given map_id is returned.
     * @param context The current context
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @return void
     */
    public static void mapGetMovableObjectsList(Context context, final SuccessCallback callback, final ErrorCallback failure) {
        mapGetMovableObjectsList(context, callback, failure, null, null);
    };

    /**
     * Returns on success all base vectors used as walls, detected by the robot sensor. If the optional
     * parameter map_id is passed and the robot supports "permanent map" the room layout of the given map_id is returned.
     * @param context The current context
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @param user_id User unique id created when registering to b2b server. Only mandatory when using an IOT connection to the robot.
     * @param robot_id Available via robotGetRobotId. Only mandatory when using an IOT connection to the robot.
     * @return void
     */
    public static void mapGetRoomLayout(Context context, final SuccessCallback callback, final ErrorCallback failure, String user_id, String robot_id) {
        GET(context, null, "get/feature_map", null, callback, failure, user_id, robot_id);
    };

    /**
     * Returns on success all base vectors used as walls, detected by the robot sensor. If the optional
     * parameter map_id is passed and the robot supports "permanent map" the room layout of the given map_id is returned.
     * @param context The current context
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @return void
     */
    public static void mapGetRoomLayout(Context context, final SuccessCallback callback, final ErrorCallback failure) {
        mapGetRoomLayout(context, callback, failure, null, null);
    };

    /**
     * Returns on success the cleaing grid stored on the robot..
     * @param context The current context
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @param user_id User unique id created when registering to b2b server. Only mandatory when using an IOT connection to the robot.
     * @param robot_id Available via robotGetRobotId. Only mandatory when using an IOT connection to the robot.
     * @return void
     */
    public static void mapGetCleaningGrid(Context context, final SuccessCallback callback, final ErrorCallback failure, String user_id, String robot_id) {
        GET(context, null, "get/cleaning_grid_map", null, callback, failure, user_id, robot_id);
    };
    /**
     * Returns on success the cleaing grid stored on the robot..
     * @param context The current context
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @return void
     */
    public static void mapGetCleaningGrid(Context context, final SuccessCallback callback, final ErrorCallback failure) {
        mapGetCleaningGrid(context, callback, failure, null, null);
    };

    ///////////////////////////////////////////////////////////////////
    // Permanent Map Methods
    ///////////////////////////////////////////////////////////////////



    ///////////////////////////////////////////////////////////////////
    // Permanent Map Methods set requests
    ///////////////////////////////////////////////////////////////////


    /**
     * The robot will plan a path and try to move to the given point.
     * The map id indicates to which map the location refers to.
     * @param context The current context
     * @param parameters map_id, x1, y1
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @param user_id User unique id created when registering to b2b server. Only mandatory when using an IOT connection to the robot.
     * @param robot_id Available via robotGetRobotId. Only mandatory when using an IOT connection to the robot.
     */
    public static void actionMoveToTargetPoint(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure, String user_id, String robot_id){
        ArrayList<String> toIntegerKeysList = new ArrayList<String>();
        toIntegerKeysList.add("x1");
        toIntegerKeysList.add("y1");
        parameters = convertToFloat(parameters, toIntegerKeysList);
        GET(context, parameters, "set/target_point", null, callback, failure, user_id, robot_id);
    }

    /**
     * The robot will plan a path and try to move to the given point.
     * The map id indicates to which map the location refers to.
     * @param context The current context
     * @param parameters map_id, x1, y1
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     */
    public static void actionMoveToTargetPoint(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure){
        actionMoveToTargetPoint(context, parameters, callback, failure, null, null);
    }

    /**
     * Explores a map
     * @param context The current context
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @param user_id User unique id created when registering to b2b server. Only mandatory when using an IOT connection to the robot.
     * @param robot_id Available via robotGetRobotId. Only mandatory when using an IOT connection to the robot.
     */
    public static void actionExplore(Context context, final SuccessCallback callback, final ErrorCallback failure, String user_id, String robot_id){
        GET(context, new JSONObject(), "set/explore", null, callback, failure, user_id, robot_id);
    }

    /**
     * Explores a map
     * @param context The current context
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     */
    public static void actionExplore(Context context, final SuccessCallback callback, final ErrorCallback failure){
        actionExplore(context, callback, failure, null, null);
    }



    /**
     * Cleans to map specified by map_id.
     * @param context The current context
     * @param parameters map_id, suction intensity
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @param user_id User unique id created when registering to b2b server. Only mandatory when using an IOT connection to the robot.
     * @param robot_id Available via robotGetRobotId. Only mandatory when using an IOT connection to the robot.
     */
    public static void actionCleanMap(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure, String user_id, String robot_id) {
        if(parameters.length() == 0){
            try {
                parameters.put("cleaning_parameter_set", 0);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        GET(context, parameters, "set/clean_map", null, callback, failure,user_id, robot_id);
    }

    /**
     * Cleans to map specified by map_id.
     * @param context The current context
     * @param parameters map_id, suction intensity
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     */
    public static void actionCleanMap(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure) {
        actionCleanMap(context, parameters, callback, failure, null, null);
    }


    /**
     * Cleans specified area or sequence of areas if multiple areas are passed in the parameters
     * @param context The current context
     * @param parameters map_id, area_id, (optional multiple area_ids), suction intensity
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @param user_id User unique id created when registering to b2b server. Only mandatory when using an IOT connection to the robot.
     * @param robot_id Available via robotGetRobotId. Only mandatory when using an IOT connection to the robot.
     */
    public static void actionCleanArea(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure, String user_id, String robot_id) {
        GET(context, parameters, "set/clean_area", null, callback, failure,user_id, robot_id);
    }

    /**
     * Cleans specified area or sequence of areas if multiple areas are passed in the parameters
     * @param context The current context
     * @param parameters map_id, area_id, (opt)area_id2 ... area_idn, suction intensity
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     */
    public static void actionCleanArea(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure) {
        actionCleanArea(context, parameters, callback, failure, null, null);
    }


    /**
     * An area will be added to the current map. In the same request the points that define the area will be included,
     as well as the area type (“room”, “blocking”, “to_be_cleaned”) The robot
     will assign the area id.
     Also a userdefined name can be given.
     * @param context The current context
     * @param parameters map_id, area_meta_data, area_type, optional cleaning_parameter_set, x1, x2, …, xn,yn
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @param user_id User unique id created when registering to b2b server. Only mandatory when using an IOT connection to the robot.
     * @param robot_id Available via robotGetRobotId. Only mandatory when using an IOT connection to the robot.
     */
    public static void robotAddArea(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure, String user_id, String robot_id){
        GET(context, parameters, "set/add_area", null, callback, failure,user_id, robot_id);
    }

    /**
     * An area will be added to the current map. In the same request the points that define the area will be included,
     as well as the area type (“room”, “blocking”, “to_be_cleaned”) The robot
     will assign the area id.
     Also a userdefined name can be given.
     * @param context The current context
     * @param parameters map_id, area_meta_data, area_type, optional cleaning_parameter_set, x1, x2, …, xn,yn
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     */
    public static void robotAddArea(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure){
        robotAddArea(context, parameters, callback, failure, null, null);
    }


    /**
     * An area will be modified to the specified input parameters.
     * @param context The current context
     * @param parameters map_id, area_meta_data, area_type, optional cleaning_parameter_set, x1, x2, …, xn,yn
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @param user_id User unique id created when registering to b2b server. Only mandatory when using an IOT connection to the robot.
     * @param robot_id Available via robotGetRobotId. Only mandatory when using an IOT connection to the robot.
     */
    public static void robotModifyArea(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure, String user_id, String robot_id){
        GET(context, parameters, "set/modify_area", null, callback, failure,user_id, robot_id);
    }


    /**
     * An area will be modified to the specified input parameters.
     * @param context The current context
     * @param parameters map_id, area_meta_data, area_type, optional cleaning_parameter_set, x1, x2, …, xn,yn
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     */
    public static void robotModifyArea(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure){
        robotModifyArea(context, parameters, callback, failure, null, null);
    }

    /**
     * Two or more areas will be merged into
     one. The area_type indicates the type
     of the new merged area.
     * @param context The current context
     * @param parameters map_id, area_meta_data, area_type, <cleaning_parameter_set>, area_id1, area_id2 … area_idn
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @param user_id User unique id created when registering to b2b server. Only mandatory when using an IOT connection to the robot.
     * @param robot_id Available via robotGetRobotId. Only mandatory when using an IOT connection to the robot.
     */
    public static void robotMergeAreas(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure, String user_id, String robot_id){
        GET(context, parameters, "set/merge_areas", null, callback, failure,user_id, robot_id);
    }

    /**
     * Two or more areas will be merged into
     one. The area_type indicates the type
     of the new merged area.
     * @param context The current context
     * @param parameters map_id, area_meta_data, area_type, <cleaning_parameter_set>, area_id1, area_id2 … area_idn
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     */
    public static void robotMergeAreas(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure){
        robotMergeAreas(context, parameters, callback, failure, null, null);
    }


    /**
     * Deletes the area identified by map and area id
     * @param context The current context
     * @param parameters map_id, area_id
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @param user_id User unique id created when registering to b2b server. Only mandatory when using an IOT connection to the robot.
     * @param robot_id Available via robotGetRobotId. Only mandatory when using an IOT connection to the robot.
     */
    public static void robotDeleteArea(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure, String user_id, String robot_id){
        GET(context, parameters, "set/delete_area", null, callback, failure,user_id, robot_id);
    }

    /**
     * Deletes the area identified by map and area id
     * @param context The current context
     * @param parameters map_id, area_id
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     */
    public static void robotDeleteArea(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure){
        robotDeleteArea(context, parameters, callback, failure, null, null);
    }

    /**
     * Divides an area in two areas. The segment described by the points x1/y1 x2/y2 defines the split of the area
     with area_id.
     * @param context The current context
     * @param parameters map_id, area_id, x1, y1, x2, y2
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @param user_id User unique id created when registering to b2b server. Only mandatory when using an IOT connection to the robot.
     * @param robot_id Available via robotGetRobotId. Only mandatory when using an IOT connection to the robot.
     */
    public static void robotSplitArea(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure, String user_id, String robot_id){
        GET(context, parameters, "set/split_area", null, callback, failure,user_id, robot_id);
    }


    /**
     * Divides an area in two areas. The segment described by the points x1/y1 x2/y2 defines the split of the area
     with area_id.
     * @param context The current context
     * @param parameters map_id, area_id, x1, y1, x2, y2
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     */
    public static void robotSplitArea(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure){
        robotSplitArea(context, parameters, callback, failure, null, null);
    }

    /**
     * If map_id is non-zero, it will be used
     for all requests (like /get/tile_map)
     that take an optional map id. These methods are: mapGetRoomLayout, mapGetMovableObjectsList, mapGetAreas, mapGetTileMap, mapGetDoorMap, robotSaveMap
     * @param context The current context
     * @param parameters map_id
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @param user_id User unique id created when registering to b2b server. Only mandatory when using an IOT connection to the robot.
     * @param robot_id Available via robotGetRobotId. Only mandatory when using an IOT connection to the robot.
     */
    public static void robotSetUsedMap(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure, String user_id, String robot_id){
        GET(context, parameters, "set/used_map", null, callback, failure,user_id, robot_id);
    }

    /**
     * If map_id is non-zero, it will be used
     for all requests (like /get/tile_map) that take an optional map id. These methods are: mapGetRoomLayout, mapGetMovableObjectsList, mapGetAreas, mapGetTileMap, mapGetDoorMap, robotSaveMap
     * @param context The current context
     * @param parameters map_id
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     */
    public static void robotSetUsedMap(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure){
        robotSetUsedMap(context, parameters, callback, failure, null, null);
    }



    /**
     * Modifies the given map (exchanges the map's metadata)
     * @param context The current context
     * @param parameters map_id, map_meta_data
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @param user_id User unique id created when registering to b2b server. Only mandatory when using an IOT connection to the robot.
     * @param robot_id Available via robotGetRobotId. Only mandatory when using an IOT connection to the robot.
     */
    public static void mapModifyMap(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure, String user_id, String robot_id){
        GET(context, parameters, "set/modify_map", null, callback, failure,user_id, robot_id);
    }

    /**
     * Modifies the given map (exchanges the map's metadata)
     * @param context The current context
     * @param parameters map_id, map_meta_data
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     */
    public static void mapModifyMap(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure){
        mapModifyMap(context, parameters, callback, failure, null, null);
    }

    /**
     * Saves the given map. (If map_id is omitted, it will save the "current" map.)
     * @param context The current context
     * @param parameters map_id
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @param user_id User unique id created when registering to b2b server. Only mandatory when using an IOT connection to the robot.
     * @param robot_id Available via robotGetRobotId. Only mandatory when using an IOT connection to the robot.
     */
    public static void robotSaveMap(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure, String user_id, String robot_id) {
        GET(context, parameters, "set/save_map", null, callback, failure, user_id, robot_id);
    }

    /**
     * Saves the given map. If map_id is omitted, it will save the "current" map.
     * @param context The current context
     * @param parameters map_id
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     */
    public static void robotSaveMap(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure) {
        robotSaveMap(context, parameters, callback, failure, null, null);
    }


    /**
     * Deletes an existing map.
     * @param context The current context
     * @param parameters map_id
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @param user_id User unique id created when registering to b2b server. Only mandatory when using an IOT connection to the robot.
     * @param robot_id Available via robotGetRobotId. Only mandatory when using an IOT connection to the robot.
     */
    public static void robotDeleteMap(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure, String user_id, String robot_id) {
        GET(context, parameters, "set/delete_map", null, callback, failure, user_id, robot_id);
    }

    /**
     * Deletes an existing map.
     * @param context The current context
     * @param parameters map_id
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     */
    public static void robotDeleteMap(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure) {
        robotDeleteMap(context, parameters, callback, failure, null, null);
    }


    ///////////////////////////////////////////////////////////////////
    // Permanent Map Methods get requests
    ///////////////////////////////////////////////////////////////////

    /**
     * Returns the id of the currently active map on the robot
     * @param context The current context
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @param user_id User unique id created when registering to b2b server. Only mandatory when using an IOT connection to the robot.
     * @param robot_id Available via robotGetRobotId. Only mandatory when using an IOT connection to the robot.
     */
    public static void robotGetCurrentMapId(Context context, final SuccessCallback callback, final ErrorCallback failure, String user_id, String robot_id){
        mapGetRobotPosition(context, new RobartSdk.SuccessCallback(){
                    @Override
                    public void onSuccess(String result){
                        String map_id = null;
                        JSONObject response = new JSONObject();
                        try {
                            JSONObject resultJSON = new JSONObject(result);
                            map_id = resultJSON.getString("map_id");
                            response.put("map_id", map_id);
                            callback.onSuccess(response.toString());
                        } catch (JSONException e) {
                            failure.onError(e.toString());
                        }
                    }
                },
                new RobartSdk.ErrorCallback(){
                    @Override
                    public void onError(String result){
                        failure.onError(result);
                    }
                },user_id, robot_id);
    }

    /**
     * Returns the id of the currently active map on the robot
     * @param context The current context
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     */
    public static void robotGetCurrentMapId(Context context, final SuccessCallback callback, final ErrorCallback failure){
        robotGetCurrentMapId(context, callback, failure, null, null);
    }

    /**
     * If map_id is not provided, data for the default map_id are returned.
     * Area_id is an integer which identifies an area (can be used for deleting / modifying an area).
     * Points are interconnected by lines, the last point is connected to the first one. A valid area must
     * contain at least 3 points. The area_type indicates the type of area (“room”, “to_be_cleaned”, “blocking”).
     * area_meta_data is any userdefined string to name the Area. It does not have to be unique.
     * @param context The current context
     * @param parameters optional map_id
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @param user_id User unique id created when registering to b2b server. Only mandatory when using an IOT connection to the robot.
     * @param robot_id Available via robotGetRobotId. Only mandatory when using an IOT connection to the robot.
     */
    public static void mapGetAreas(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure, String user_id, String robot_id) {
        GET(context, parameters, "get/areas", null, callback, failure, user_id, robot_id);
    }

    /**
     * If map_id is not provided, data for the default map_id are returned.
     * Area_id is an integer which identifies an area (can be used for deleting / modifying an area).
     * Points are interconnected by lines, the last point is connected to the first one. A valid area must
     * contain at least 3 points. The area_type indicates the type of area (“room”, “to_be_cleaned”, “blocking”).
     * area_meta_data is any userdefined string to name the Area. It does not have to be unique.
     * @param context The current context
     * @param parameters optional map_id
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     */
    public static void mapGetAreas(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure) {
        mapGetAreas(context, parameters, callback, failure, null, null);
    }


    /**
     * If map_id is not provided, data for the default map_id are returned.
     * A map consists of a set of area ids that belong to the simplified map. These areas can be retrieved by the mapGetArea request.
     * Additionally, the map consists of a set of simplified lines that indicated mayor obstacles (segments). All areas and segments are represented in the same (global) coordinate system.
     * Finally, a transformation is suggested, which indicates a possible map transformation for simplified displying.
     * @param context The current context
     * @param parameters optional map_id
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @param user_id User unique id created when registering to b2b server. Only mandatory when using an IOT connection to the robot.
     * @param robot_id Available via robotGetRobotId. Only mandatory when using an IOT connection to the robot.
     */
    public static void mapGetTileMap(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure, String user_id, String robot_id) {
        GET(context, parameters, "get/tile_map", null, callback, failure, user_id, robot_id);
    }

    /**
     * If map_id is not provided, data for the default map_id are returned.
     * A map consists of a set of area ids that belong to the simplified map. These areas can be retrieved by the mapGetArea request.
     * Additionally, the map consists of a set of simplified lines that indicated mayor obstacles (segments). All areas and segments are represented in the same (global) coordinate system.
     * Finally, a transformation is suggested, which indicates a possible map transformation for simplified displying.
     * @param context The current context
     * @param parameters optional map_id
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     */
    public static void mapGetTileMap(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure) {
        mapGetTileMap(context, parameters, callback, failure, null, null);
    }


    /**
     * If map_id is not provided, data for the default map_id are returned.
     * The door map shows the lines of the estimated doors in the tile_map: x1/y1 is the starting point, x2/y2 is the end point.
     * The suggested_orientation transformation indicates a possible transformation of this map for simplified displaying.
     * @param context The current context
     * @param parameters optional map_id
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @param user_id User unique id created when registering to b2b server. Only mandatory when using an IOT connection to the robot.
     * @param robot_id Available via robotGetRobotId. Only mandatory when using an IOT connection to the robot.
     */
    public static void mapGetDoorMap(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure, String user_id, String robot_id) {
        GET(context, parameters, "get/door_map", null, callback, failure, user_id, robot_id);
    }

    /**
     * If map_id is not provided, data for the default map_id are returned.
     * The door map shows the lines of the estimated doors in the tile_map: x1/y1 is the starting point, x2/y2 is the end point.
     * The suggested_orientation transformation indicates a possible transformation of this map for simplified displaying.
     * @param context The current context
     * @param parameters optional map_id
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     */
    public static void mapGetDoorMap(Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure) {
        mapGetDoorMap(context, parameters, callback, failure, null, null);
    }

    /**
     * Returns all available maps on the robot including meta_data and map related statistics.
     * @param context The current context
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @param user_id User unique id created when registering to b2b server. Only mandatory when using an IOT connection to the robot.
     * @param robot_id Available via robotGetRobotId. Only mandatory when using an IOT connection to the robot.
     */
    public static void robotGetMaps(Context context, final SuccessCallback callback, final ErrorCallback failure, String user_id, String robot_id) {
        GET(context, new JSONObject(), "get/maps", null, callback, failure, user_id, robot_id);
    }

    /**
     * Returns all available maps on the robot including meta_data and map related statistics.
     * @param context The current context
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     */
    public static void robotGetMaps(Context context, final SuccessCallback callback, final ErrorCallback failure) {
        robotGetMaps(context, callback, failure, null, null);
    }


    ///////////////////////////////////////////////////////////////////
    // end of Permanent Map Methods
    ///////////////////////////////////////////////////////////////////



    /**
     * Does a call to the api endpoint to get current firmware version available.
     * @param context The current context
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @param user_id User unique id created when registering to b2b server.
     * @return void
     */
    private static void getToken(final Context context, String firmware, final String unique_id, final SuccessCallback callback, final ErrorCallback failure, final String user_id) {
        System.out.println("Authenticating...");
        final JSONObject parameters = new JSONObject();
        currentFirmware = firmware;
        lastUniqueId = unique_id;
        try {
            parameters.put("username", "user");
            parameters.put("password", "user");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        POST(context, parameters, firmwareURL+"api/authenticate",
                new RobartSdk.SuccessCallback(){
                    @Override
                    public void onSuccess(String result){
                        System.out.println("Authentication completed, getting current version...");
                        String token = null;
                        String newEndpoint = "api/robots/byUID/<uID>/version";
                        try {
                            JSONObject resultJSON = new JSONObject(result);
                            token = resultJSON.getString("token");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        String finalEndpoint = newEndpoint.replaceAll("<uID>", unique_id);
                        JSONObject parameters = new JSONObject();
                        lastToken = token;
                        GET(context, parameters, finalEndpoint, token, callback, failure, user_id, null);
                    }
                },
                new RobartSdk.ErrorCallback(){
                    @Override
                    public void onError(String result){
                        failure.onError(result.toString());
                    }
                });
    };

    /**
     * Internal method used to validate if the firmware can be upgraded
     * @param context The current context
     * @param parameters Json object with parameters, refer to documentation for details.
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @param user_id User unique id created when registering to b2b server.
     * @return void
     */
    private static void validateVersion(final Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure, final String user_id) {
        System.out.println("Checking robot version...");
        robotGetRobotId(context, parameters,
                new RobartSdk.SuccessCallback(){
                    @Override
                    public void onSuccess(String result){
                        String firmware = null;
                        String unique_id = null;
                        try {
                            JSONObject resultJSON = new JSONObject(result);
                            firmware = resultJSON.getString("firmware");
                            unique_id = resultJSON.getString("unique_id");
                        } catch (JSONException e) {
                            failure.onError(e.toString());
                        }
                        getToken(context, firmware, unique_id, callback, failure, user_id);
                    }
                },
                new RobartSdk.ErrorCallback(){
                    @Override
                    public void onError(String result){
                        failure.onError(result);
                    }
                },
                user_id, null
        );
    }

    /**
     * Does a call to the robot to get its current firmware version.
     * @param context The current context
     * @param parameters Json object with parameters, refer to documentation for details.
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @param user_id User unique id created when registering to b2b server. Only mandatory when using an IOT connection to the robot.
     * @param robot_id Available via robotGetRobotId. Only mandatory when using an IOT connection to the robot.
     * @return void
     */
    public static void checkFirmwareVersion(final Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure, String user_id, String robot_id) {
        System.out.println("Checking robot version...");
        robotGetRobotId(context, parameters,
                new RobartSdk.SuccessCallback(){
                    @Override
                    public void onSuccess(String result){
                        String version = null;
                        JSONObject response = new JSONObject();
                        try {
                            JSONObject resultJSON = new JSONObject(result);
                            version = resultJSON.getString("firmware");
                            response.put("version", version);
                            callback.onSuccess(response.toString());
                        } catch (JSONException e) {
                            failure.onError(e.toString());
                        }

                    }
                },
                new RobartSdk.ErrorCallback(){
                    @Override
                    public void onError(String result){
                        failure.onError(result);
                    }
                },
                user_id, robot_id
        );
    }

    /**
     * Does a call to the FW server to get current firmware version available.
     * @param context The current context
     * @param parameters Json object with parameters, refer to documentation for details.
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @param user_id User unique id created when registering to b2b server.
     * @return void
     */
    public static void robotGetVersion(final Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure, String user_id) {
        validateVersion(context, parameters, callback, failure, user_id);
    };

    /**
     * Download the last available firmware to the device and executes the provided callback.
     * @param context The current context
     * @param parameters Json object with parameters, refer to documentation for details.
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @param user_id User unique id created when registering to b2b server. Only mandatory when using an IOT connection to the robot.
     * @param robot_id Available via robotGetRobotId. Only mandatory when using an IOT connection to the robot.
     * @return void
     */
    public static void downloadFirmware(final Context context, JSONObject parameters, final SuccessCallback callback, final ErrorCallback failure, String user_id, String robot_id) {
        validateVersion(context, parameters,
                new RobartSdk.SuccessCallback(){
                    @Override
                    public void onSuccess(String result){
                        String newFirmware = null;
                        try {
                            JSONObject resultJSON = new JSONObject(result);
                            newFirmware = resultJSON.getString("version");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if (currentFirmware != newFirmware){
                            String newEndpoint = firmwareURL + "api/robots/byUID/<uID>/firmware";
                            String finalEndpoint = newEndpoint.replaceAll("<uID>", lastUniqueId);
                            //DOWNLOAD QUICK TEST
                            //finalEndpoint = "http://speedtest.ftp.otenet.gr/files/test100k.db";
                            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(finalEndpoint));
                            request.setDescription("Robart firmware update version:" + newFirmware);
                            request.setTitle("FW Update");
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                                request.allowScanningByMediaScanner();
                                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                            }
                            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "robot_fw.http");
                            DownloadManager manager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
                            request.addRequestHeader("X-Auth-Token", lastToken);
                            manager.enqueue(request);
                            BroadcastReceiver onComplete = new BroadcastReceiver() {
                                public void onReceive(Context ctxt, Intent intent) {
                                    JSONObject parameters = new JSONObject();
                                    try {
                                        parameters.put("message", "Firmware downloaded");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    callback.onSuccess(parameters.toString());
                                }
                            };
                            context.registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
                        }

                    }
                },
                new RobartSdk.ErrorCallback(){
                    @Override
                    public void onError(String result){
                        failure.onError(result);
                    }
                },
                user_id
        );
    };

    /**
     * Installs the downloaded firmware to the robot and then executes the provided callback
     * @param context The current context
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @param user_id User unique id created when registering to b2b server. Only mandatory when using an IOT connection to the robot.
     * @param robot_id Available via robotGetRobotId. Only mandatory when using an IOT connection to the robot.
     * @return void
     */
    public static void installFirmware(final Context context, final SuccessCallback callback, final ErrorCallback failure, String user_id, String robot_id) {
        String endpoint = robotEndpoint+"post/firmware";
        String fileuri = Environment.getExternalStorageDirectory().toString() + "/" + Environment.DIRECTORY_DOWNLOADS + "/robot_fw.http";
        new UploadFilesTask(new UploadFilesTask.TaskListener() {
            @Override
            public void onFinished(Integer result) {
                JSONObject parameters = new JSONObject();
                if (result == 200) {
                    try {
                        parameters.put("message", "Installation completed");
                        parameters.put( "status", result);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    callback.onSuccess(parameters.toString());
                } else if (result == -200) {
                    try {
                        parameters.put("error", "File not found");
                        parameters.put( "status", result);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    failure.onError(parameters.toString());
                } else {
                    try {
                        parameters.put("error", "Installation failed");
                        parameters.put( "status", result);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    failure.onError(parameters.toString());
                }
            }
        }).execute(fileuri, endpoint);
    };

    /**
     * An AsyncTask wrapper used to resolve the firmware upload to the robot.
     */
    private static class UploadFilesTask extends AsyncTask<String, Void, Integer> {

        /**
         * Public interface to execute actions from the main thread when the upload is completed
         */
        public interface TaskListener {
            public void onFinished(Integer result);
        }

        private final TaskListener taskListener;

        /**
         * Class constructor containing the upload task
         */
        public UploadFilesTask(TaskListener listener) {
            this.taskListener = listener;
        }

        /**
         * Install the download firmware to the robot and then executes the provided callback
         * @param params An array with two params, the file endpoint [0] and upload endpoint [1]
         * @return Integer
         */
        @Override
        protected Integer doInBackground(String... params) {
            String fileName = params[0];
            HttpURLConnection conn = null;
            OutputStream dos = null;
            String lineEnd = "\r\n";
            String twoHyphens = "--";
            String boundary = "*****";
            int bytesRead, bytesAvailable, bufferSize;
            byte[] buffer;
            int maxBufferSize = 1 * 1024 * 1024;
            File sourceFile = new File(params[0]);
            if (!sourceFile.isFile()) {
                return -200;
            }
            else
            {
                try {
                    FileInputStream fileInputStream = new FileInputStream(sourceFile);
                    URL url = new URL(params[1]);
                    conn = (HttpURLConnection) url.openConnection();
                    conn.setDoInput(true);
                    conn.setDoOutput(true);
                    conn.setUseCaches(false);
                    conn.setRequestMethod("POST");
                    dos = conn.getOutputStream();
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    buffer = new byte[bufferSize];
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                    while (bytesRead > 0) {
                        dos.write(buffer, 0, bufferSize);
                        bytesAvailable = fileInputStream.available();
                        bufferSize = Math.min(bytesAvailable, maxBufferSize);
                        bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                    }
                    fileInputStream.close();
                    dos.flush();
                    InputStream is = conn.getInputStream();
                    return conn.getResponseCode();
                } catch (MalformedURLException ex) {
                    System.out.println(ex);
                    return 0;
                } catch (Exception e) {
                    System.out.println(e);
                    return 0;
                }
            }
        }

        /**
         * Default function for AsyncTask to handle thread completion.
         * @param result Upload request status result
         * @return Integer
         */
        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            if(this.taskListener != null) {
                this.taskListener.onFinished(result);
            }
        }

    }

    /**
     * Converts a provided JSONObject to a query string to be used on and http request
     * @param parameters a JSONObject containing the parameters to be converted to query string
     * @return String
     */
    public static String toQueryString(JSONObject parameters) {
        Integer index = 0;
        String key;
        StringBuilder urlParameters = new StringBuilder();
        urlParameters.append("?");
        Iterator<String> prop = parameters.keys();
        while (prop.hasNext()) {
            key = prop.next();
            if (index > 0) {
                urlParameters.append("&");
            }
            try {
                urlParameters.append(String.format("%s=%s",
                        URLEncoder.encode(key, "UTF-8"),
                        URLEncoder.encode(parameters.get(key).toString(), "UTF-8")
                ));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            index++;
        }
        return urlParameters.toString();
    };

    /**
     * Converts the requested attibutes (on keyList) to integer and returns the modified JSONObject
     * @param parameters A JSONObject list of parameters
     * @param keyList An ArrayList with the keys to be converted to int
     * @return JSONObject
     */
    public static JSONObject convertToFloat(JSONObject parameters, ArrayList keyList) {
        Iterator<String> prop = parameters.keys();
        String key;
        Integer newInt;
        while (prop.hasNext()) {
            key = prop.next();
            try {
                if (keyList.contains(key)) {
                    newInt = Math.round(Float.parseFloat(parameters.get(key).toString()));
                    parameters.put(key, newInt);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return parameters;
    };

    /**
     * Returns a Callback resolving the response from the robot status.
     * @param context The current context
     * @param parameters Json object with parameters to be sended over the request
     * @param token Any token to be added to the request head
     * @param callback Callback to be executed on success
     * @param user_id User unique id created when registering to b2b server. Only mandatory when using an IOT connection to the robot.
     * @param robot_id Available via robotGetRobotId. Only mandatory when using an IOT connection to the robot.
     * @return void
     */
    private static void GET(Context context, JSONObject parameters, String api, String token, final SuccessCallback callback, final ErrorCallback failure, String user_id, String robot_id) {
        final RequestQueue queue = Volley.newRequestQueue(context);

        String endpoint;
        String apiMainThreecharacters = api.substring(0, 3);
        String auth = null;

        if (parameters == null) {
            parameters = new JSONObject();
        }

        if (apiMainThreecharacters.equals("api")) {
            endpoint = firmwareURL;
        } else {
            if (user_id == null){
                endpoint = robotEndpoint;
            } else {
                if(PLTSK == null) {
                    failure.onError("Not registered to the iOT server");
                    return;
                }
                endpoint = iOTEndpoint;
                if(robot_id != null){
                    try {
                        parameters.put("robot_id", robot_id);
                    } catch (JSONException e) {
                        failure.onError("could not add robot_id to the parameters");
                        return;
                    }
                }

                List nonAuthEndpoints = Arrays.asList("iot/add_device", "iot/auth");

                if(!nonAuthEndpoints.contains(api)) {
                    auth = "Basic " + Base64.encodeToString(String.format("%s:%s",user_id,PLTSK).getBytes(), Base64.NO_WRAP);
                }

            }
        }
        endpoint += api;

        if (parameters.length() > 0){
            endpoint += toQueryString(parameters);
        }
        final String finalToken = token;
        final String finalAuth = auth;
        HttpsTrustManager.allowAllSSL();
        final StringRequest stringRequest = new StringRequest(Request.Method.GET, endpoint,
                new Response.Listener<String>() {
                    public String result;
                    @Override
                    public void onResponse(String response) {
                        result = response;
                        callback.onSuccess(result);
                    }
                },
                new Response.ErrorListener() {
                    public String result;
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        if(volleyError.networkResponse != null && volleyError.networkResponse.data != null){
                            VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                            volleyError = error;
                        }
                        result = volleyError.getMessage();
                        failure.onError(result);
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                if (finalToken != null) {
                    params.put("X-Auth-Token", finalToken);
                }
                if (finalAuth != null) {
                    params.put("Authorization", finalAuth);
                }

                return params;
            }
        };
        int socketTimeout = 900000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        queue.add(stringRequest);
    };

    /**
     * Returns a Callback resolving the response from the robot status.
     * @param context The current context
     * @param parameters Json object with parameters to be sended over the request
     * @param endpoint The target url endpoint for this request
     * @param callback Callback to be executed on success
     * @param failure Callback to be executed on failure
     * @return void
     */
    private static void POST(Context context, final JSONObject parameters, String endpoint, final SuccessCallback callback, final ErrorCallback failure) {
        RequestQueue queue = Volley.newRequestQueue(context);
        HttpsTrustManager.allowAllSSL();
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, endpoint,
                new Response.Listener<String>() {
                    public String result;
                    @Override
                    public void onResponse(String response) {
                        result = response;
                        callback.onSuccess(result);
                    }
                },
                new Response.ErrorListener() {
                    public String result;
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        if(volleyError.networkResponse != null && volleyError.networkResponse.data != null){
                            VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                            volleyError = error;
                        }
                        result = volleyError.getMessage();
                        failure.onError(result);
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                //params.put("username", "user");
                //params.put("password", "user");
                Iterator<String> iter = parameters.keys();
                while (iter.hasNext()) {
                    String key = iter.next();
                    String value = null;
                    try {
                        value = parameters.get(key).toString();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    params.put(key, value);
                }
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };
        int socketTimeout = 900000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        queue.add(stringRequest);
    };

    /**
     * A public interface to execute SuccessCallbacks from the main thread.
     * @return void
     */
    public interface SuccessCallback{
        void onSuccess(String result);
    }

    /**
     * A public interface to execute ErrorCallbacks from the main thread.
     * @return void
     */
    public interface ErrorCallback{
        void onError(String result);
    }
}